# Blockchain Rust

## Introduction

Permet la lecture de fichiers .dat issu de la Blockchain Bitcoin

## Sommaire
* [Introduction](#Introduction)
* [Installation](#Installation)
* [Utilisation](#Utilisation)
* [Exemples](#Exemples)
* [Bibliothèques](#Bibliothèques)
* [Sources](#Sources)

## Installation

### Deps
  * cmake
  * pkg-config
  * libssl
  * libfreetype6-dev (Debian)
  * libexpat1-dev (Debian) / libexpat
  * libxcb-composite0-dev (Debian) / libxcb (Arch Linux)

#### (Debian)

``` sudo apt install cmake ```

``` sudo apt install pkg-config libasound2-dev libssl-dev cmake libfreetype6-dev libexpat1-dev libxcb-composite0-dev ```

#### (Arch Linux)

``` sudo pacman -S cmake pkg-config libexpat libxcb ```

### Install
``` cargo install --git https://gitlab.com/AlyssaShep/blockchain-rust.git ```

## Utilisation

### - avec cargo install

```[chemin_bin_cargo_install] [choix] [dossier] [dossier]```

  * d, defaut, default                         mode par défaut (les fichiers à lire doivent être mis dans ./ins/ et seront exportés dans ./outs/)

  * o, out [chemin_sortie]                     permet de choisir le dossier d'exportation
  * i, in [chemin_entrée]                      permet de choisir le dossier contenant les fichiers à lire
  * io, inout [chemin_entrée] [chemin_sortie]  permet de choisir le dossier contenant les fichiers à lire et le dossier où exporter les résultats


### - avec git clone

```cargo run --release [choix] [dossier] [dossier]```

  * d, defaut, default                         mode par défaut (les fichiers à lire doivent être mis dans ./ins/ et seront exportés dans ./outs/)

  * o, out [chemin_sortie]                     permet de choisir le dossier d'exportation
  * i, in [chemin_entrée]                      permet de choisir le dossier contenant les fichiers à lire
  * io, inout [chemin_entrée] [chemin_sortie]  permet de choisir le dossier contenant les fichiers à lire et le dossier où exporter les résultats


## Exemples

```cargo run --release i ./ins/``` lit les fichiers .dat dans le dossier ./ins/

```cargo run --release o ./outs/``` écrit les exportations en .json/.txt dans le dossier ./outs/

```cargo run --release io ./ins/ ./outs/ ``` lit les fichiers .dat dans ./ins et écrit en .json /.txt dans ./outs/

## Bibliothèques
* [std](https://doc.rust-lang.org/std/)
* [tui](https://docs.rs/tui/latest/tui/index.html)
* [crossterm](https://docs.rs/crossterm/0.23.2/crossterm/)
* [hexstring](https://docs.rs/hexstring/0.1.3/hexstring/)
* [sha2](https://docs.rs/sha2/0.10.2/sha2/)
* [serde](https://docs.rs/serde/1.0.137/serde/)
* [serde_derive](https://docs.rs/serde_derive/1.0.137/serde_derive/)
* [strum_macros](https://docs.rs/strum_macros/0.24.0/strum_macros/)
* [chrono](https://docs.rs/chrono/0.4.19/chrono/)
* [bs58](https://docs.rs/bs58/0.4.0/bs58/)
* [regex](https://docs.rs/regex/1.5.5/regex/)
* [ripemd](https://docs.rs/ripemd/0.1.1/ripemd/)
* [primitive_types](https://docs.rs/primitive-types/0.11.1/primitive_types/)

## Sources
* [Bitcoin](https://developer.bitcoin.org/devguide/)
* [learn me a bitcoin](https://learnmeabitcoin.com/)
* [documentation Rust](https://www.rust-lang.org/learn)
* [crates.io](https://crates.io/crates)

