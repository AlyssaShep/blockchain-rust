#![allow(non_snake_case)]

extern crate hex;

use csv::Writer;

use crate::structure::{
    block_struct::Block,
    header_struct::Header,
    input_struct::Input,
    out_point::OutPoint,
    output_struct::Output,
    transaction_struct::Transaction,
};

use crate::traitements::interpretations_functions::{write_waiting, parser_waiting};

use tui::{
    backend::{Backend, CrosstermBackend},
    Terminal,
};

use crossterm::{
    event::EnableMouseCapture,
    execute,
    terminal::{enable_raw_mode, EnterAlternateScreen, ScrollDown, ScrollUp},
};

use std::{
    error::Error,
    io,
    fs,
    fs::File,
    io::{BufWriter, Write, Read},
    path::Path,
    sync::{Arc, Mutex},
    thread,
};

use sha2::{Digest, Sha256};

pub fn swap_string(string: &mut Vec<u8>) {
    let size: usize = string.len();
    let mut i: usize = 0;
    let half: usize = size / 2;

    // inversion totale
    let mut inv: isize = size as isize;
    inv -= 1;
    while i < half {
        let temp = string[i];
        string[i] = string[inv as usize];
        string[inv as usize] = temp;
        i += 1;
        inv -= 1;
    }
}

pub fn varint_recovery(input_count: Vec<u8>) -> u64 {
    type Array = [u8; 8];

    let mut array_input_count: Array = [0; 8];
    for i in 0..input_count.len() {
        array_input_count[i] = input_count[i];
    }

    let result: u64 = u64::from_le_bytes(array_input_count);

    return result;
}

pub fn header_recovery(buffer: &Vec<u8>, id_file: usize, result: &mut Header) -> usize {
    let mut i = 0;
    let mut dec: usize = id_file as usize;
    let read_bytes: usize;

    let mut version: Vec<u8> = Vec::with_capacity(4);
    let mut previous_block_hash: Vec<u8> = Vec::with_capacity(32);
    let mut merkle_root: Vec<u8> = Vec::with_capacity(32);
    let mut time: Vec<u8> = Vec::with_capacity(4);
    let mut bits: Vec<u8> = Vec::with_capacity(4);
    let mut nonce: Vec<u8> = Vec::with_capacity(4);

    // recup version
    let size_version: usize = 4;
    while i < size_version {
        version.push(buffer[dec]);
        i += 1;
        dec += 1;
    }
    i = 0;

    // recup previous block hash
    let size_previous_hash: usize = 32;
    while i < size_previous_hash {
        previous_block_hash.push(buffer[dec]);
        i += 1;
        dec += 1;
    }
    i = 0;

    // recup merkle root
    let size_merkle_root: usize = 32;
    while i < size_merkle_root {
        merkle_root.push(buffer[dec]);
        i += 1;
        dec += 1;
    }
    i = 0;

    // recup time
    let size_time: usize = 4;
    while i < size_time {
        time.push(buffer[dec]);
        i += 1;
        dec += 1;
    }
    i = 0;

    // recup bits
    let size_bits: usize = 4;
    while i < size_bits {
        bits.push(buffer[dec]);
        i += 1;
        dec += 1;
    }
    i = 0;

    // recup nonce
    let size_nonce: usize = 4;
    while i < size_nonce {
        nonce.push(buffer[dec]);
        i += 1;
        dec += 1;
    }

    // Conversion des octets lu pour les stocker dans les attributs de la structure Header
    type Array = [u8; 4];

    let mut array_version: Array = [0; 4];
    for i in 0..version.len() {
        array_version[i] = version[i];
    }

    let version_h: i32 = i32::from_le_bytes(array_version);

    let mut array_time: Array = [0; 4];
    for i in 0..time.len() {
        array_time[i] = time[i];
    }

    let time_h: u32 = u32::from_le_bytes(array_time);

    let mut array_bits: Array = [0; 4];
    for i in 0..bits.len() {
        array_bits[i] = bits[i];
    }

    let bits_h: u32 = u32::from_le_bytes(array_bits);

    let mut array_nonce: Array = [0; 4];
    for i in 0..nonce.len() {
        array_nonce[i] = nonce[i];
    }

    let nonce_h: u32 = u32::from_le_bytes(array_nonce);

    swap_string(&mut previous_block_hash);

    swap_string(&mut merkle_root);

    let header: Header = Header::new(
        version_h,
        previous_block_hash,
        merkle_root,
        time_h,
        bits_h,
        nonce_h,
    );

    *result = header;

    read_bytes = dec - id_file;

    return read_bytes;
}

pub fn inputs_recovery(buffer: &Vec<u8>, id_file: usize, result: &mut Vec<Input>) -> usize {
    let mut i = 0;
    let mut dec: usize = id_file as usize;
    let read_bytes: usize;
    type Array = [u8; 4];

    // lecture d un specifique Output
    let mut hash_vout: Vec<u8> = Vec::with_capacity(32);
    while i < 32 {
        hash_vout.push(buffer[dec]);
        i += 1;
        dec += 1;
    }
    swap_string(&mut hash_vout);

    let mut index_vout: Vec<u8> = Vec::with_capacity(4);
    i = 0;
    while i < 4 {
        index_vout.push(buffer[dec]);
        i += 1;
        dec += 1;
    }

    let mut array_index_vout: Array = [0; 4];
    for i in 0..index_vout.len() {
        array_index_vout[i] = index_vout[i];
    }

    let index_vout_t: u32 = u32::from_le_bytes(array_index_vout);

    // lecture de la taille du script
    let mut script_sig_size: Vec<u8> = Vec::with_capacity(1);
    script_sig_size.push(buffer[dec]);
    dec += 1;
    i = 0;

    if script_sig_size[0] == 253 {
        script_sig_size.remove(0);
        while i < 2 {
            script_sig_size.push(buffer[dec]);
            i += 1;
            dec += 1;
        }
    } else if script_sig_size[0] == 254 {
        script_sig_size.remove(0);
        while i < 4 {
            script_sig_size.push(buffer[dec]);
            i += 1;
            dec += 1;
        }
    } else if script_sig_size[0] == 255 {
        script_sig_size.remove(0);
        while i < 8 {
            script_sig_size.push(buffer[dec]);
            i += 1;
            dec += 1;
        }
    }

    let script_sig_size_t: u64 = varint_recovery(script_sig_size);

    // lecture du script
    let mut script_sig: Vec<u8> = Vec::with_capacity(script_sig_size_t as usize);
    i = 0;
    while i < script_sig_size_t {
        script_sig.push(buffer[dec]);
        i += 1;
        dec += 1;
    }

    swap_string(&mut script_sig);

    // sequence
    let mut sequence: Vec<u8> = Vec::with_capacity(4);
    i = 0;
    while i < 4 {
        sequence.push(buffer[dec]);
        i += 1;
        dec += 1;
    }

    let mut array_sequence: Array = [0; 4];
    for i in 0..sequence.len() {
        array_sequence[i] = sequence[i];
    }

    let sequence_t: u32 = u32::from_le_bytes(array_sequence);

    // creation de la structure Outpoint
    let outpoint: OutPoint = OutPoint::new(hash_vout, index_vout_t);

    // creation de l input a ajouter
    let input: Input = Input::new(outpoint, script_sig_size_t as u32, script_sig, sequence_t);

    // ajout du resultat

    result.push(input);

    read_bytes = dec - id_file;

    return read_bytes;
}

pub fn outputs_recovery(buffer: &Vec<u8>, id_file: usize, result: &mut Vec<Output>) -> usize {
    let mut i = 0;
    let mut dec: usize = id_file as usize;
    let read_bytes: usize;
    type Array = [u8; 8];

    // lecture de la valeur
    let mut value: Vec<u8> = Vec::with_capacity(8);
    while i < 8 {
        value.push(buffer[dec]);
        i += 1;
        dec += 1;
    }

    let mut array_value: Array = [0; 8];
    for i in 0..value.len() {
        array_value[i] = value[i];
    }

    let value_t: i64 = i64::from_le_bytes(array_value);

    // lecture de la taille de la clef
    let mut script_pub_key_size: Vec<u8> = Vec::with_capacity(1);
    script_pub_key_size.push(buffer[dec]);
    dec += 1;
    i = 0;

    if script_pub_key_size[0] == 253 {
        script_pub_key_size.remove(0);
        while i < 2 {
            script_pub_key_size.push(buffer[dec]);
            i += 1;
            dec += 1;
        }
    } else if script_pub_key_size[0] == 254 {
        script_pub_key_size.remove(0);
        while i < 4 {
            script_pub_key_size.push(buffer[dec]);
            i += 1;
            dec += 1;
        }
    } else if script_pub_key_size[0] == 255 {
        script_pub_key_size.remove(0);
        while i < 8 {
            script_pub_key_size.push(buffer[dec]);
            i += 1;
            dec += 1;
        }
    }

    let size_key: u64 = varint_recovery(script_pub_key_size);

    // lecture de la cle
    let mut key: Vec<u8> = Vec::with_capacity(size_key as usize);
    i = 0;

    while i < size_key {
        key.push(buffer[dec]);
        i += 1;
        dec += 1;
    }

    swap_string(&mut key);

    // creation du output
    let output: Output = Output::new(value_t, size_key, key);

    result.push(output);

    read_bytes = dec - id_file;

    return read_bytes;
}

pub fn transactions_recovery(
    buffer: &Vec<u8>,
    id_file: usize,
    result: &mut Vec<Transaction>,
 ) -> usize {
    let mut i = 0;
    let mut dec: usize = id_file as usize;
    type Array = [u8; 4];

    let mut version: Vec<u8> = Vec::with_capacity(4);
    while i < 4 {
        version.push(buffer[dec]);
        i += 1;
        dec += 1;
    }

    let mut array_version: Array = [0; 4];
    for i in 0..version.len() {
        array_version[i] = version[i];
    }

    let version_t: i32 = i32::from_le_bytes(array_version);

    let mut input_count: Vec<u8> = Vec::with_capacity(1);
    input_count.push(buffer[dec]);
    dec += 1;
    i = 0;

    if input_count[0] == 253 {
        input_count.remove(0);
        while i < 2 {
            input_count.push(buffer[dec]);
            i += 1;
            dec += 1;
        }
    } else if input_count[0] == 254 {
        input_count.remove(0);
        while i < 4 {
            input_count.push(buffer[dec]);
            i += 1;
            dec += 1;
        }
    } else if input_count[0] == 255 {
        input_count.remove(0);
        while i < 8 {
            input_count.push(buffer[dec]);
            i += 1;
            dec += 1;
        }
    }

    let input_count_t: u64 = varint_recovery(input_count);

    let mut inputs: Vec<Input> = Vec::with_capacity(input_count_t as usize);
    i = 0;
    while i < input_count_t {
        dec += inputs_recovery(&buffer, dec, &mut inputs);
        i += 1;
    }

    let mut output_count: Vec<u8> = Vec::with_capacity(1);
    output_count.push(buffer[dec]);
    dec += 1;
    i = 0;

    if output_count[0] == 253 {
        output_count.remove(0);
        while i < 2 {
            output_count.push(buffer[dec]);
            i += 1;
            dec += 1;
        }
    } else if output_count[0] == 254 {
        output_count.remove(0);
        while i < 4 {
            output_count.push(buffer[dec]);
            i += 1;
            dec += 1;
        }
    } else if output_count[0] == 255 {
        output_count.remove(0);
        while i < 8 {
            output_count.push(buffer[dec]);
            i += 1;
            dec += 1;
        }
    }

    let output_count_t: u64 = varint_recovery(output_count);

    let mut outputs: Vec<Output> = Vec::with_capacity(output_count_t as usize);
    i = 0;
    while i < output_count_t {
        dec += outputs_recovery(&buffer, dec, &mut outputs);
        i += 1;
    }

    let mut locktime: Vec<u8> = Vec::with_capacity(4);
    i = 0;
    while i < 4 {
        locktime.push(buffer[dec]);
        dec += 1;
        i += 1;
    }

    let mut array_locktime: Array = [0; 4];
    for i in 0..locktime.len() {
        array_locktime[i] = locktime[i];
    }

    let locktime_t: u32 = u32::from_le_bytes(array_locktime);

    let trans: Transaction = Transaction::new(
        version_t,
        input_count_t as u64,
        inputs,
        output_count_t as u64,
        outputs,
        locktime_t,
    );

    result.push(trans);

    let read_bytes = dec - id_file;

    return read_bytes;
}

pub fn read_file(
    name_file: &String,
    blocks: &mut Vec<Block>,
    size_file: u64,
 ) -> Result<usize, std::io::Error> {
    let mut fichier = File::open(name_file)?;

    let mut buffer = Vec::with_capacity(size_file as usize);

    fichier.read_to_end(&mut buffer)?;

    let size_file = buffer.len();
    let mut id_file: usize = 0;

    while id_file < size_file {
        let mut magic_bytes: Vec<u8> = Vec::with_capacity(4);
        let mut size: Vec<u8> = Vec::with_capacity(4);

        let mut i: u64 = 0;
        while i < 4 {
            magic_bytes.push(buffer[id_file]);
            i += 1;
            id_file += 1;
        }

        let mb: String = hex::encode(magic_bytes);

        i = 0;
        while i < 4 {
            size.push(buffer[id_file]);
            i += 1;
            id_file += 1;
        }

        type Array = [u8; 4];
        let mut array_size: Array = [0; 4];
        for i in 0..size.len() {
            array_size[i] = size[i];
        }

        let size_block: i32 = i32::from_le_bytes(array_size);

        // recuperation du header
        let mut header: Header = Default::default();

        id_file += header_recovery(&buffer, id_file, &mut header);

        // recup nb de transactions
        let mut tx_count: Vec<u8> = Vec::with_capacity(1);
        tx_count.push(buffer[id_file]);
        id_file += 1;
        i = 0;

        if tx_count[0] == 253 {
            tx_count.remove(0);
            while i < 2 {
                tx_count.push(buffer[id_file]);
                i += 1;
                id_file += 1;
            }
        } else if tx_count[0] == 254 {
            tx_count.remove(0);
            while i < 4 {
                tx_count.push(buffer[id_file]);
                i += 1;
                id_file += 1;
            }
        } else if tx_count[0] == 255 {
            tx_count.remove(0);
            while i < 8 {
                tx_count.push(buffer[id_file]);
                i += 1;
                id_file += 1;
            }
        }

        let tx_count_t: u64 = varint_recovery(tx_count);

        // recup des transactions
        let mut transactions: Vec<Transaction> = Default::default();
        i = 0;

        while i < tx_count_t {

            let id_temp = id_file;
            id_file += transactions_recovery(&buffer, id_file, &mut transactions);

            let size_buffer: usize = id_file - id_temp;

            // on recupere les informations de transactions pour faire l id de transactions
            let mut tx_id_buffer: Vec<u8> = Vec::with_capacity(size_buffer);
            for id in id_temp..id_file {
                tx_id_buffer.push(buffer[id]);
            }

            // passe dans la fonction SHA256

            let mut hasher = Sha256::new();
            hasher.update(&tx_id_buffer);
            let res = hasher.finalize();
            let mut hasher2 = Sha256::new();
            hasher2.update(&res);

            let mut temp = hasher2.finalize().to_vec();
            swap_string(&mut temp);

            Transaction::set_tx_id(&mut transactions[i as usize], temp);

            i += 1;
        }

        // creation du block
        let block: Block = Block::new(mb, size_block, header, tx_count_t as i32, transactions)?;

        blocks.push(block);
    }

    return Ok(id_file);
}

pub fn write_txt<B: Backend>(
    name_file: &String,
    blocks: &Vec<Block>,
    terminal: &mut Terminal<B>,
 ) -> Result<(), std::io::Error> {

    let mut file = BufWriter::new(File::create(name_file).unwrap());
    let mut val_loop: u16;
    let mut progress: usize = 0;

    for block in blocks {
        write!(file, "{:?}", block)?;
        progress += 1;
        val_loop = (progress * 100 / blocks.len()) as u16;
        terminal.draw(|f| write_waiting(f, &mut val_loop, name_file.clone()))?;
    }

    return Ok(());
}

pub fn write_csv<B: Backend>(
    name_file: &String,
    blocks: &Vec<Block>,
    terminal: &mut Terminal<B>,
 ) -> Result<(), Box<dyn Error>> {

    let mut val_loop: u16;
    let mut progress: usize = 0;

    let mut wtr = Writer::from_path(Path::new(name_file))?;

    for block in blocks {
        wtr.serialize(block)?;

        progress += 1;
        val_loop = (progress * 100 / blocks.len()) as u16;
        terminal.draw(|f| write_waiting(f, &mut val_loop, name_file.clone()))?;
    }

    return Ok(());
}

pub fn write_json<B: Backend>(
    name_file: &String,
    blocks: &Vec<Block>,
    terminal: &mut Terminal<B>,
 ) -> Result<(), Box<dyn Error>> {

    let mut file = BufWriter::new(File::create(name_file).unwrap());
    let mut val_loop: u16;
    let mut progress: usize = 0;

    for block in blocks {
        write!(file, "{}", serde_json::to_string(block).unwrap())?;

        progress += 1;
        val_loop = (progress * 100 / blocks.len()) as u16;
        terminal.draw(|f| write_waiting(f, &mut val_loop, name_file.clone()))?;
    }

    return Ok(());
}

pub fn write_adresses(
    name_file: &String,
    blocks: &Vec<Block>,
 ) -> Result<(), std::io::Error> {
    let len_block = blocks.len();
    let res = len_block / 100;
    let mut cpt = 0;

    let mut file = File::create(name_file)?;
    for block in blocks {
        write!(file, "{:?}", block)?;

        //afficher avancée du traitement en % avec l'heure
        if cpt % res == 0 && (cpt / res) % 20 == 0 {
            println!("Ecriture -> {}%", cpt / res);
        }
        cpt += 1;
    }

    println!("{}/{} ", cpt, len_block);
    return Ok(());
}

pub fn block_number_file(
    name_file: &String,
    block_number: &mut i32,
 ) -> Result<usize, std::io::Error> {
    let mut file = File::open(name_file)?;

    let mut buffer = Vec::new();

    file.read_to_end(&mut buffer)?;

    let size_file = buffer.len();
    let mut id_file: usize = 0;

    let mut blk_nb : i32 = 0;

    while id_file < size_file {

        let mut size: Vec<u8> = Vec::with_capacity(4);

        let mut i: u64 = 0;
        while i < 4 {
            i += 1;
            id_file += 1;
        }

        i = 0;
        while i < 4 {
            size.push(buffer[id_file]);
            i += 1;
            id_file += 1;
        }

        type Array = [u8; 4];
        let mut array_size: Array = [0; 4];
        for i in 0..size.len() {
            array_size[i] = size[i];
        }
        let size_block: i32 = i32::from_le_bytes(array_size);

        id_file += size_block as usize;

        blk_nb += 1;
    }

    *block_number = blk_nb;

    return Ok(id_file);
}

pub fn secure_block_number(name_file: &String) -> Result<i32, std::io::Error> {

    let mut block_number: i32 = 0 as i32;
    let block_number_file: usize = block_number_file(name_file, &mut block_number)?;

    let metadata_file = fs::metadata(name_file)?;
    let size_file: u64 = metadata_file.len();

    if size_file != block_number_file as u64 {
        return Err(std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            "ERREUR : Le nombre d'octets lu n'est pas le bon.",
        ));
    }

    return Ok(block_number);
}

pub fn secure_read_file(
    name_file: &String,
    blocks: &mut Vec<Block>,
 ) -> Result<(), std::io::Error> {
    // let maintenant = Instant::now();

    let metadata_file = fs::metadata(&String::from(name_file)).unwrap();
    let read_file: usize = read_file(name_file, blocks, metadata_file.len()).unwrap();

    let metadata_file = fs::metadata(name_file)?;
    let size_file: u64 = metadata_file.len();

    if size_file != read_file as u64 {
        return Err(std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            "ERREUR : Le nombre d'octets lu n'est pas le bon.",
        ));
    }

    return Ok(());
}

pub fn block_size_file(
    name_file: &String,
    blocks_size: &mut Vec<usize>,
 ) -> Result<usize, std::io::Error> {
    let mut fichier = File::open(name_file)?;

    let mut buffer = Vec::new();

    fichier.read_to_end(&mut buffer)?;

    let size_file = buffer.len();
    let mut id_file: usize = 0;

    let mut blocks_size_temp : Vec<usize> = Vec::new();

    while id_file < size_file {

        let mut size: Vec<u8> = Vec::with_capacity(4);

        let mut i: u64 = 0;
        while i < 4 {
            i += 1;
            id_file += 1;
        }

        i = 0;
        while i < 4 {
            size.push(buffer[id_file]);
            i += 1;
            id_file += 1;
        }

        type Array = [u8; 4];
        let mut array_size: Array = [0; 4];
        for i in 0..size.len() {
            array_size[i] = size[i];
        }
        let size_block: i32 = i32::from_le_bytes(array_size);

        id_file += size_block as usize;

        blocks_size_temp.push(size_block as usize + 8);
    }

    *blocks_size = blocks_size_temp;

    return Ok(id_file);
}

pub fn secure_block_size_file(name_file: &String) -> Result<Vec<usize>, std::io::Error> {

    let mut blocks_size: Vec<usize> = Vec::new();
    let block_size_file: usize = block_size_file(name_file, &mut blocks_size)?;

    let metadata_file = fs::metadata(name_file)?;
    let size_file: u64 = metadata_file.len();

    if size_file != block_size_file as u64 {
        return Err(std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            "ERREUR : Le nombre d'octets lu n'est pas le bon.",
        ));
    }

    return Ok(blocks_size);
}

pub fn fragmented_read_file<B: Backend>(
    id_start: usize,
    id_end: usize,
    buffer: Arc<Vec<u8>>,
    mutex_blocks: Arc<Mutex<Vec<Block>>>,
    mutex_cmpt: Arc<Mutex<usize>>,
    mutex_val_loop: Arc<Mutex<u16>>,
    mutex_terminal: Arc<Mutex<Terminal<B>>>,
    mutex_nb_blk_total_clone: Arc<Mutex<usize>>,
    file : Arc<String>,
 ) -> Result<usize, std::io::Error> {
    let mut id_start_local: usize = id_start;
    let id_end_local: usize = id_end;

    while id_start_local < id_end_local {
        let mut magic_bytes: Vec<u8> = Vec::with_capacity(4);
        let mut size: Vec<u8> = Vec::with_capacity(4);

        let mut i: u64 = 0;
        while i < 4 {
            magic_bytes.push(buffer[id_start_local]);
            i += 1;
            id_start_local += 1;
        }

        let mb: String = hex::encode(magic_bytes);

        i = 0;
        while i < 4 {
            size.push(buffer[id_start_local]);
            i += 1;
            id_start_local += 1;
        }

        type Array = [u8; 4];
        let mut array_size: Array = [0; 4];
        for i in 0..size.len() {
            array_size[i] = size[i];
        }
        let size_block: i32 = i32::from_le_bytes(array_size);

        // recuperation du header
        let mut header: Header = Default::default();

        id_start_local += header_recovery(&buffer, id_start_local, &mut header);

        // recup nb de transactions
        let mut tx_count: Vec<u8> = Vec::with_capacity(1);
        tx_count.push(buffer[id_start_local]);
        id_start_local += 1;
        i = 0;

        if tx_count[0] == 253 {
            tx_count.remove(0);
            while i < 2 {
                tx_count.push(buffer[id_start_local]);
                i += 1;
                id_start_local += 1;
            }
        } else if tx_count[0] == 254 {
            tx_count.remove(0);
            while i < 4 {
                tx_count.push(buffer[id_start_local]);
                i += 1;
                id_start_local += 1;
            }
        } else if tx_count[0] == 255 {
            tx_count.remove(0);
            while i < 8 {
                tx_count.push(buffer[id_start_local]);
                i += 1;
                id_start_local += 1;
            }
        }

        let tx_count_t: u64 = varint_recovery(tx_count);

        // recup des transactions
        let mut transactions: Vec<Transaction> = Default::default();
        i = 0;
        while i < tx_count_t {
            let id_temp = id_start_local;
            id_start_local += transactions_recovery(&buffer, id_start_local, &mut transactions);

            let size_buffer: usize = id_start_local - id_temp;
            let mut tx_id_buffer: Vec<u8> = Vec::with_capacity(size_buffer);
            for id in id_temp..id_start_local {
                tx_id_buffer.push(buffer[id]);
            }

            // passe dans la fonction SHA256

            let mut hasher = Sha256::new();
            hasher.update(&tx_id_buffer);
            let res = hasher.finalize();
            let mut hasher2 = Sha256::new();
            hasher2.update(&res);

            Transaction::set_tx_id(&mut transactions[i as usize], hasher2.finalize().to_vec());

            i += 1;
        }

        // creation du block
        let block: Block = Block::new(mb, size_block, header, tx_count_t as i32, transactions)?;

        let _ = &mutex_blocks.lock().unwrap().push(block);
        let mut blk_nb = mutex_cmpt.lock().unwrap();
        *blk_nb += 1;

        let nb_blk_total = mutex_nb_blk_total_clone.lock().unwrap();

        let mut val_l = mutex_val_loop.lock().unwrap();
        *val_l = (*blk_nb * 100 / *nb_blk_total) as u16;
    }

    let mut terminal = mutex_terminal.lock().unwrap();
    let mut val_l = mutex_val_loop.lock().unwrap();

    terminal.draw(|f| parser_waiting(f, &mut val_l, file.to_string()))?;

    return Ok(id_start_local);
}

pub fn read_file_threads(
    name_file: &String,
    blocks: &mut Vec<Block>,
 ) -> Result<usize, std::io::Error> {
    enable_raw_mode()?;
    let mut stdout = io::stdout();

    execute!(
        stdout,
        EnterAlternateScreen,
        EnableMouseCapture,
        ScrollUp(1000),
        ScrollDown(1000)
    )?;

    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    let threads_number: usize = 15; // Nombre de threads, pour le moment créé comme constante locale à cette fonction.

    let blocks_size: Vec<usize> = secure_block_size_file(name_file)?; // Le tableau de longueurs des blocks en bytes du fichier que l'on veut lire.

    let block_number: usize = 0;

    let mut val_loop: u16 = 0;

    terminal.draw(|f| parser_waiting(f, &mut val_loop, name_file.clone()))?;

    if blocks_size.len() < threads_number
    // Si jamais le nombre de blocks dans un fichier est inférieur au nombre de threads,
    {
        // alors il n'y a pas besoin de créer de threads et le fichier est lu dans le thread principal.
        secure_read_file(name_file, blocks)?;

        let metadata_file = fs::metadata(name_file)?;
        let size_file: u64 = metadata_file.len();

        return Ok(size_file as usize);
    }

    let mut tab_threads_handlers = vec![]; // Le tableau de threads.

    let number_blocks_per_threads = blocks_size.len() / threads_number; // Le nombre de blocks traités par threads, excepté pour le dernier thread
                                                                              // qui devra traité le nombre de blocks restant, qui peut être différent.

    let mut fichier = File::open(name_file)?;

    let mut buffer = Vec::new();

    fichier.read_to_end(&mut buffer).unwrap();

    let blk_result: Vec<Block> = Vec::with_capacity(blocks_size.len());

    let mutex_blocks = Arc::new(Mutex::new(blk_result));

    let buffer_to_pass = Arc::new(buffer);

    // Implémentation pour le compteur de blocks.

    // Déclaration des variables nécessaires.

    let mutex_cmpt = Arc::new(Mutex::new(block_number));

    let nb_blk_total: usize = blocks_size.len();

    let mutex_val_loop = Arc::new(Mutex::new(val_loop));

    let mutex_terminal = Arc::new(Mutex::new(terminal));

    let fichier_th = Arc::new(name_file.clone());

    let mutex_nb_blk_total = Arc::new(Mutex::new(nb_blk_total));

    // Fin des déclarations des variables nécessaires.

    let mut id_start: usize = 0;
    let mut id_end: usize = 0;

    for i in 0..threads_number {
        if i < (threads_number - 1) {
            for j in (i * number_blocks_per_threads)
                ..(number_blocks_per_threads + (i * number_blocks_per_threads))
            {
                id_end += blocks_size[j];
            }
        } else {
            for j in (i * number_blocks_per_threads)..blocks_size.len() {
                id_end += blocks_size[j];
            }
        }

        let buffer_to_pass_clone = Arc::clone(&buffer_to_pass);
        let mutex_blocks_clone = Arc::clone(&mutex_blocks);
        let mutex_cmpt_clone = Arc::clone(&mutex_cmpt);
        let mutex_val_loop_clone = Arc::clone(&mutex_val_loop);
        let mutex_terminal_clone = Arc::clone(&mutex_terminal);
        let mutex_nb_blk_total_clone = Arc::clone(&mutex_nb_blk_total);
        let fichier_clone = Arc::clone(&fichier_th);

        tab_threads_handlers.push(thread::spawn(move || {
            fragmented_read_file(
                id_start,
                id_end,
                buffer_to_pass_clone,
                mutex_blocks_clone,
                mutex_cmpt_clone,
                mutex_val_loop_clone,
                mutex_terminal_clone,
                mutex_nb_blk_total_clone,
                fichier_clone,
            )
            .unwrap();
        }));

        id_start = id_end;
    }

    for processus in tab_threads_handlers {
        let _truc = match processus.join() {
            Ok(truc) => truc,
            Err(_e) => {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                    "ERREUR : Le nombre d'octets lu n'est pas le bon.",
                ))
            }
        };
    }

    let blk_result = mutex_blocks.lock().unwrap();

    for i in 0..blk_result.len() {
        blocks.push(blk_result[i].clone());
    }

    let metadata_file = fs::metadata(name_file)?;
    let size_file: u64 = metadata_file.len();

    return Ok(size_file as usize);
}

pub fn write_vec_file<T: std::fmt::Debug>(
    name_file: &String,
    tab: &Vec<T>,
 ) -> Result<(), std::io::Error> {
    let mut file = File::create(name_file).unwrap();

    for i in 0..tab.len() {
        write!(file, "{:?} : {:?}\n", i, tab[i]).unwrap();
    }

    return Ok(());
}
