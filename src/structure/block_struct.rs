use serde_derive::Serialize;

use crate::structure::{
    header_struct::Header,
    transaction_struct::Transaction
};

use crate::traitements::conversions::satoshi_to_bitcoin;

use strum_macros::ToString;

use std::{cmp::Ordering, fmt};

#[derive(Clone, ToString, Serialize)]
pub enum MagicBytes {
    MainNet(String),
    TestNet3(String),
    RegTest(String),
}

impl fmt::Debug for MagicBytes {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self {
            MagicBytes::MainNet(_String) => write!(f, "MainNet"),
            MagicBytes::TestNet3(_String) => write!(f, "TestNet3"),
            MagicBytes::RegTest(_String) => write!(f, "RegTest"),
        }
    }
}

#[derive(Clone, Serialize)]
pub struct Block {
    magic_bytes: MagicBytes,
    size: i32,
    block_header: Header,
    tx_count: i32,
    transaction: Vec<Transaction>,
}

impl fmt::Debug for Block {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {

        write!(f, "[Block : {:?} \n size {:?} \n block header {:?} \n nb transactions {:?} \n transactions {:?}] \n", self.magic_bytes, self.size, self.block_header, self.tx_count, self.transaction)
    }
}

impl Block {
    pub fn new(
        mb: String,
        size: i32,
        block_header: Header,
        tx_count: i32,
        transaction: Vec<Transaction>,
    ) -> Result<Block, std::io::Error> {
        let magic_bytes: MagicBytes = match mb.as_str() {
            "f9beb4d9" => MagicBytes::MainNet(String::from("f9beb4d9")),
            "0b110907" => MagicBytes::TestNet3(String::from("0b110907")),
            "fabfb5da" => MagicBytes::RegTest(String::from("fabfb5da")),
            _ => {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::InvalidInput,
                    "invalid magic bytes",
                ))
            }
        };

        return Ok(Block {
            magic_bytes,
            size,
            block_header,
            tx_count,
            transaction,
        });
    }

    pub fn get_magic_bytes(&self) -> &MagicBytes {
        return &self.magic_bytes;
    }

    pub fn get_block_header(&self) -> &Header {
        &self.block_header
    }

    pub fn average_value(&self) -> f64 {
        let mut avg: f64 = 0.0;
        let mut sum_tx: f64 = 0.0;

        for it in &self.transaction {
            for ito in it.outputs() {
                avg += *ito.value() as f64;
                sum_tx += 1.0;
            }
        }

        avg /= sum_tx;

        return satoshi_to_bitcoin(avg as i64);
    }

    pub fn stat(
        &self,
        quartile_1: &mut f64,
        mediane: &mut f64,
        quartile_3: &mut f64,
        volume: &mut f64,
    ) {
        let mut value_data: Vec<&i64> = Vec::new();
        let mut vol: i64 = 0;

        for i in self.transaction() {
            for j in i.outputs() {
                value_data.push(j.value());
                vol += *j.value();
            }
        }

        value_data.sort_by(|a, b| {
            if a < b {
                Ordering::Less
            } else if a == b {
                Ordering::Equal
            } else {
                Ordering::Greater
            }
        });

        let rang_mediane: usize = ((value_data.len() as f64) / 2.0) as usize;
        let quartile_rang: usize = ((value_data.len() as f64) / 4.0) as usize;
        let quartile_3_rang: usize = ((3.0 * (value_data.len() as f64)) / 4.0) as usize;

        *mediane = satoshi_to_bitcoin(*value_data[rang_mediane]);
        *quartile_1 = satoshi_to_bitcoin(*value_data[quartile_rang]);
        *quartile_3 = satoshi_to_bitcoin(*value_data[quartile_3_rang]);
        *volume = satoshi_to_bitcoin(vol);
    }

    pub fn size(&self) -> &i32 {
        return &self.size;
    }

    pub fn tx_count(&self) -> &i32 {
        return &self.tx_count;
    }

    pub fn transaction(&self) -> &Vec<Transaction> {
        return &self.transaction;
    }
}
