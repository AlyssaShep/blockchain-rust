use crate::structure::{
    input_struct::Input,
    output_struct::Output,
};

use serde_derive::Serialize;

use hex_string::HexString;
use std::default::Default;
use std::fmt;

use serde::Serializer;

#[derive(Clone, Serialize)]
pub struct Transaction {
    #[serde(serialize_with = "string_vec_u8")]
    tx_id: Vec<u8>,
    version: i32,
    input_count: u64,
    inputs: Vec<Input>,
    output_count: u64,
    outputs: Vec<Output>,
    locktime: u32,
}

pub fn swap_string_serialize(tx_id: &Vec<u8>) -> Vec<u8> {
    let size: usize = tx_id.len();
    let mut i: usize = 0;
    let half: usize = size / 2;
    let mut res: Vec<u8> = Vec::with_capacity(size);

    for _ in 0..size {
        res.push(0);
    }

    // inversion totale
    let mut inv: isize = size as isize;
    inv -= 1;
    while i < half {
        res[i] = tx_id[inv as usize];
        res[inv as usize] = tx_id[i];
        i += 1;
        inv -= 1;
    }
    return res;
}

fn string_vec_u8<S>(x: &Vec<u8>, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    s.collect_str(&HexString::from_bytes(&swap_string_serialize(x)).as_string())
}

impl Default for Transaction {
    fn default() -> Transaction {
        return Transaction {
            tx_id: Default::default(),
            version: 0,
            input_count: 0,
            inputs: Default::default(),
            output_count: 0,
            outputs: Default::default(),
            locktime: 0,
        };
    }
}

impl fmt::Debug for Transaction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let tx_id_inverse: Vec<u8> = swap_string_serialize(&self.tx_id);
        write!(f, "[Transactions : tx_id {:?} \n version {:?} \n nb input {:?} \n inputs {:?} \n nb outputs {:?} \n outptus {:?} \n locktime {:?}]\n", HexString::from_bytes(&tx_id_inverse).as_string(), self.version, self.input_count, self.inputs, self.output_count, self.outputs, self.locktime)
    }
}

impl Transaction {
    pub fn new(
        version: i32,
        input_count: u64,
        inputs: Vec<Input>,
        output_count: u64,
        outputs: Vec<Output>,
        locktime: u32,
    ) -> Transaction {
        return Transaction {
            tx_id: Default::default(),
            version,
            input_count,
            inputs,
            output_count,
            outputs,
            locktime,
        };
    }

    pub fn version(&self) -> &i32 {
        return &self.version;
    }

    pub fn output_count(&self) -> &u64 {
        return &self.output_count;
    }

    pub fn input_count(&self) -> &u64 {
        return &self.input_count;
    }

    pub fn set_tx_id(&mut self, tx: Vec<u8>) {
        self.tx_id = tx;
    }

    pub fn tx_id_to_string(&self) -> String {
        return HexString::from_bytes(&self.tx_id).as_string();
    }

    pub fn tx_id(&self) -> &Vec<u8> {
        return &self.tx_id;
    }

    pub fn outputs(&self) -> &Vec<Output> {
        return &self.outputs;
    }

    pub fn inputs(&self) -> &Vec<Input> {
        return &self.inputs;
    }

    pub fn all_values_outputs(&self) -> i64 {
        let mut res: i64 = 0;

        for i in 0..self.outputs.len() {
            res += self.outputs[i].value();
        }

        return res;
    }
}
