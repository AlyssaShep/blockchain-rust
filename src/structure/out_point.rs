use hex_string::HexString;
use std::default::Default;
use std::fmt;

use serde::Serializer;
use serde_derive::Serialize;

#[derive(Clone, Serialize)]
pub struct OutPoint {
    #[serde(serialize_with = "string_vec_u8")]
    hash: Vec<u8>,
    index: u32,
}

fn string_vec_u8<S>(x: &Vec<u8>, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    s.collect_str(&HexString::from_bytes(&x).as_string())
}

impl Default for OutPoint {
    fn default() -> OutPoint {

        return OutPoint {
            hash: Default::default(),
            index: 0,
        };
    }
}

impl fmt::Debug for OutPoint {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "[OutPoint : \n hash {:?} \n index {:?} \n]",
            self.hash_to_string(),
            self.index
        )
    }
}

impl OutPoint {
    pub fn new(hash: Vec<u8>, index: u32) -> OutPoint {
        return OutPoint { hash, index };
    }

    pub fn hash_to_string(&self) -> String {
        return HexString::from_bytes(&self.hash).as_string();
    }

    pub fn get_index(&self) -> u32 {
        self.index
    }

    pub fn index(&self) -> &u32 {
        return &self.index;
    }
}
