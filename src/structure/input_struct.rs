use crate::structure::out_point::OutPoint;
use std::default::Default;

use serde_derive::Serialize;

use hex_string::HexString;
use std::fmt;

use serde::Serializer;

#[derive(Clone, Serialize)]
pub struct Input {
    vout: OutPoint,
    script_sig_size: u32,
    #[serde(serialize_with = "string_vec_u8")]
    script_sig: Vec<u8>,
    sequence: u32,
}

impl Default for Input {
    fn default() -> Input {

        return Input {
            vout: Default::default(),
            script_sig_size: 0,
            script_sig: Default::default(),
            sequence: 0,
        };
    }
}

fn string_vec_u8<S>(x: &Vec<u8>, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    s.collect_str(&HexString::from_bytes(&x).as_string())
}

impl fmt::Debug for Input {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "[Input : vout {:?} \n script sig size {:?} \n script sig {:?} \n sequence {:?} \n]",
            self.vout,
            self.script_sig_size,
            self.script_sig_to_string(),
            self.sequence
        )
    }
}

impl Input {
    pub fn new(vout: OutPoint, script_sig_size: u32, script_sig: Vec<u8>, sequence: u32) -> Input {
        return Input {
            vout,
            script_sig_size,
            script_sig,
            sequence,
        };
    }

    pub fn get_vout(&self) -> &OutPoint {
        return &self.vout;
    }

    pub fn script_sig_to_string(&self) -> String {
        return HexString::from_bytes(&self.script_sig).as_string();
    }
}
