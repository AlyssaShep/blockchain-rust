use std::default::Default;
use std::string::String;

use std::fmt;

use chrono::prelude::*;

use serde_derive::Serialize;

use chrono::Utc;
use std::time::{Duration, UNIX_EPOCH};

use hex_string::HexString;

use serde::Serializer;

#[derive(Clone, Serialize)]
pub struct Header {
    version: i32,
    #[serde(serialize_with = "string_vec_u8")]
    previous_block_hash: Vec<u8>,
    #[serde(serialize_with = "string_vec_u8")]
    merkle_root: Vec<u8>,
    time: u32,
    bits: u32,
    nonce: u32,
}

fn string_vec_u8<S>(x: &Vec<u8>, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    s.collect_str(&HexString::from_bytes(&x).as_string())
}

impl Default for Header {
    fn default() -> Header {
        return Header {
            version: 0,
            previous_block_hash: Default::default(),
            merkle_root: Default::default(),
            time: 0,
            bits: 0,
            nonce: 0,
        };
    }
}

impl fmt::Debug for Header {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {

        let d = UNIX_EPOCH + Duration::from_secs(self.time as u64);

        let date = DateTime::<Utc>::from(d);

        let datetime = date.format("%Y-%m-%d %H:%M:%S.%f").to_string();

        write!(f, "[Header : \n version {:?} \n previous block hash {:?} \n merkle root {:?} \n time {:?} \n bits {:?} \n nonce {:?} \n]", self.version, self.block_hash_to_string(), self.merkle_root_to_string(), datetime, self.bits, self.nonce)
    }
}

impl Header {
    pub fn new(
        version: i32,
        previous_block_hash: Vec<u8>,
        merkle_root: Vec<u8>,
        time: u32,
        bits: u32,
        nonce: u32,
    ) -> Header {
        return Header {
            version,
            previous_block_hash,
            merkle_root,
            time,
            bits,
            nonce,
        };
    }

    pub fn get_merkle_root(&self) -> &Vec<u8> {
        return &self.merkle_root;
    }

    pub fn block_hash_to_string(&self) -> String {
        return HexString::from_bytes(&self.previous_block_hash).as_string();
    }

    pub fn merkle_root_to_string(&self) -> String {
        return HexString::from_bytes(&self.merkle_root).as_string();
    }

    pub fn afficher_horaire(self) -> chrono::DateTime<chrono::Utc> {

        let d = UNIX_EPOCH + Duration::from_secs(self.time as u64);

        // Create DateTime from SystemTime
        let datetime = DateTime::<Utc>::from(d);

        datetime
    }

    pub fn get_time(&self) -> &u32 {
        &self.time
    }

    pub fn get_version(&self) -> i32 {
        self.version
    }

    pub fn get_bits(&self) -> u32 {
        self.bits
    }

    pub fn get_nonce(&self) -> u32 {
        self.nonce
    }
}
