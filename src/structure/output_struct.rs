use crate::Calculs::generic_scriptpk_to_address;
use hex_string::HexString;
use std::default::Default;
use std::fmt;

use serde::Serializer;
use serde_derive::Serialize;

#[derive(Clone, Serialize)]
pub struct Output {
    value: i64,
    script_pub_key_size: u64,
    #[serde(serialize_with = "string_vec_u8")]
    script_pub_key: Vec<u8>,
}

fn string_vec_u8<S>(x: &Vec<u8>, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    s.collect_str(&HexString::from_bytes(&x).as_string())
}

impl Default for Output {
    fn default() -> Output {
        return Output {
            value: 0,
            script_pub_key_size: 0,
            script_pub_key: Default::default(),
        };
    }
}

impl fmt::Debug for Output {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "[Output : \n  value {:?} \n script pub key size {:?} \n script pub key {:?} \n Addresses {:?} \n]",
            self.value, self.script_pub_key_size, self.script_pub_key_to_string(), self.script_pub_key_to_address()
        )
    }
}

impl Output {
    pub fn new(value: i64, script_pub_key_size: u64, script_pub_key: Vec<u8>) -> Output {
        return Output {
            value,
            script_pub_key_size,
            script_pub_key,
        };
    }

    pub fn script_pub_key_to_string(&self) -> String {
        return HexString::from_bytes(&self.script_pub_key).as_string();
    }

    pub fn value(&self) -> &i64 {
        return &self.value;
    }

    pub fn script_pub_key_to_address(&self) -> Vec<String> {
        generic_scriptpk_to_address(&self.script_pub_key_to_string()).unwrap()
    }
}
