#![allow(non_snake_case)]

use crate::structure::block_struct::Block;

use std::fmt::Write;
use std::str;

use bs58;
use regex::Regex;
use ripemd::Ripemd160;
use sha2::{Digest, Sha256};
use std::collections::HashMap;

use primitive_types::U512;

pub fn parse_hex(hex_asm: &str) -> Vec<u8> {
    let hex_chars: Vec<char> = hex_asm
        .as_bytes()
        .iter()
        .filter_map(|b| {
            let ch = char::from(*b);
            if ('0' <= ch && ch <= '9') || ('a' <= ch && ch <= 'f') || ('A' <= ch && ch <= 'F') {
                Some(ch)
            } else {
                None
            }
        })
        .collect();

    let mut index = 0usize;
    let (odd_chars, even_chars): (Vec<char>, Vec<char>) = hex_chars.into_iter().partition(|_| {
        index = index + 1;
        index % 2 == 1
    });

    odd_chars
        .into_iter()
        .zip(even_chars.into_iter())
        .map(|(c0, c1)| {
            fn hexchar2int(ch: char) -> u8 {
                if '0' <= ch && ch <= '9' {
                    ch as u8 - '0' as u8
                } else {
                    0xa + if 'a' <= ch && ch <= 'f' {
                        ch as u8 - 'a' as u8
                    } else if 'A' <= ch && ch <= 'F' {
                        ch as u8 - 'A' as u8
                    } else {
                        unreachable!()
                    }
                }
            }
            hexchar2int(c0) * 0x10 + hexchar2int(c1)
        })
        .collect::<Vec<u8>>()
}

pub fn decimal_to_hexadecimal_U512(nb: U512) -> String {
    //res str
    let mut res: String = String::from("");
    let mut q: U512 = nb;
    let mut r: U512;
    let base: U512 = U512::from(16);
    let zero: U512 = U512::from(0);
    let ten: U512 = U512::from(10);
    let eleven: U512 = U512::from(11);
    let twelve: U512 = U512::from(12);
    let thirteen: U512 = U512::from(13);
    let fourteen: U512 = U512::from(14);
    while q > zero {
        r = q % base;
        q = q / base;

        let tmp_val: String = match r {
            r if r < ten => r.to_string(),
            r if r == ten => String::from("a"),
            r if r == eleven => String::from("b"),
            r if r == twelve => String::from("c"),
            r if r == thirteen => String::from("d"),
            r if r == fourteen => String::from("e"),
            _ => String::from("f"),
        };

        res += &tmp_val;
    }

    let tmp: String = res.chars().rev().collect::<String>();
    res = tmp;

    return res;
}

pub fn uncompress_pubkey(str: String) -> Result<String, std::io::Error> {
    let prefixe: String = str[0..2].to_string();
    let p: U512 = U512::from("0xfffffffffffffffffffffffffffffffffffffffffffffffffffffffefffffc2f");
    let x: U512 = U512::from_str_radix(&str[2..str.len()], 16).unwrap();
    let seven: U512 = U512::from(7);
    let four: U512 = U512::from(4);
    let two: U512 = U512::from(2);
    let one: U512 = U512::from(1);
    let zero: U512 = U512::from(0);

    //y_sq = (x^3 + 7) % p
    let mut y_sq: U512 = x % p;
    y_sq *= x;
    y_sq %= p;
    y_sq *= x;
    y_sq %= p;
    y_sq += seven;
    y_sq %= p;

    //y_sqpow((p+1)/4) / p
    let mut exposant: U512 = (p + one) / four;
    let mut y: U512 = one; //result = y
    let mut base: U512 = y_sq % p;
    //Modular exponantiation y = (base, exposant, mod)

    while exposant > U512::from(0) {
        if exposant % two == one {
            y = (y * base) % p;
        }
        exposant = exposant >> one;
        base = (base * base) % p;
    }
    let pair = y % two == zero;

    if prefixe == "02" && !pair {
        y = (p - y) % p;
    }

    if prefixe == "03" && pair {
        y = (p - y) % p;
    }
    let res_hex: String = format!(
        "{}{}{}",
        "04",
        decimal_to_hexadecimal_U512(x),
        decimal_to_hexadecimal_U512(y)
    );

    Ok(res_hex)
}

pub fn translate(script_pub_key: &String) -> Result<String, std::io::Error> {
    let script_to_bytes = parse_hex(script_pub_key);
    let mut script_reversed = String::new();
    for i in script_to_bytes.iter().rev() {
        write!(script_reversed, "{:02x}", i).unwrap();
    }

    Ok(script_reversed)
}

pub fn checksum(string: &String) -> Result<String, std::io::Error> {
    let mut hasher_256 = Sha256::new();
    hasher_256.update(hex::decode(string).expect("Invalid Hex String"));
    let hash1 = format!("{:x}", hasher_256.finalize());

    let mut hasher_256_2 = Sha256::new();
    hasher_256_2.update(hex::decode(hash1).expect("Invalid Hex String"));
    let hash2 = format!("{:x}", hasher_256_2.finalize());

    Ok(hash2[0..8].to_string())
}

pub fn pubkey_to_address(pub_key: String) -> Result<String, std::io::Error> {
    //SHA256 STEP 2
    let mut hasher_256_step_2 = Sha256::new();
    hasher_256_step_2.update(hex::decode(pub_key).expect("Invalid Hex String"));
    let hash256_step_2 = format!("{:x}", hasher_256_step_2.finalize());

    //RIPEMD160 STEP 3
    let mut hasher_160_step_3 = Ripemd160::new();
    hasher_160_step_3.update(hex::decode(hash256_step_2).expect("Invalid Hex String"));
    let ripemd160_step_3: String = format!("{:x}", hasher_160_step_3.finalize());

    //VERSION 00 STEP 4
    let mut version = String::from("00");
    version.push_str(&ripemd160_step_3);

    //SHA256 STEP 5
    let mut hasher_256_step_5 = Sha256::new();
    hasher_256_step_5.update(hex::decode(&version).expect("Invalid Hex String"));
    let hash256_step_5: String = format!("{:x}", hasher_256_step_5.finalize());

    //SHA256 STEP 6
    let mut hasher_256_step_6 = Sha256::new();
    hasher_256_step_6.update(hex::decode(hash256_step_5).expect("Invalid Hex String"));
    let mut hash256_step_6: String = format!("{:x}", hasher_256_step_6.finalize());

    //STEP 7
    hash256_step_6.truncate(8);

    //STEP 8
    version.push_str(&hash256_step_6);

    //STEP 9
    let address = bs58::encode(hex::decode(version).expect("Invalid Hex String")).into_string();

    Ok(address)
}

pub fn address_to_hash(address: String) -> Result<String, std::io::Error> {
    let hash = bs58::decode(address).into_vec().unwrap();
    let mut res = String::new();
    for i in hash.iter() {
        write!(res, "{:02x}", i).unwrap();
    }

    res.remove(0);
    res.remove(0);
    res.remove(res.len() - 1);
    res.remove(res.len() - 1);
    res.remove(res.len() - 1);
    res.remove(res.len() - 1);
    res.remove(res.len() - 1);
    res.remove(res.len() - 1);
    res.remove(res.len() - 1);
    res.remove(res.len() - 1);

    Ok(res)
}

pub fn p2pk_scriptpk_to_pubkey(script_pub_key: &String) -> Result<String, std::io::Error> {
    let mut script_reversed = translate(script_pub_key).unwrap();
    script_reversed.remove(0);
    script_reversed.remove(0);
    script_reversed.remove(script_reversed.len() - 1);
    script_reversed.remove(script_reversed.len() - 1);

    Ok(script_reversed)
}

pub fn p2pk_scriptpk_to_address(script_pub_key: &String) -> Result<String, std::io::Error> {
    Ok(pubkey_to_address(p2pk_scriptpk_to_pubkey(script_pub_key).unwrap()).unwrap())
}

pub fn p2pkh_scriptpk_to_address(script_pub_key: &String) -> Result<String, std::io::Error> {
    let mut script_reversed = translate(script_pub_key).unwrap();
    script_reversed.remove(0);
    script_reversed.remove(0);
    script_reversed.remove(0);
    script_reversed.remove(0);
    script_reversed.remove(0);
    script_reversed.remove(0);
    script_reversed.remove(script_reversed.len() - 1);
    script_reversed.remove(script_reversed.len() - 1);
    script_reversed.remove(script_reversed.len() - 1);
    script_reversed.remove(script_reversed.len() - 1);

    let mut prefixe: String = String::from("00");
    prefixe.push_str(&script_reversed);
    prefixe.push_str(&checksum(&prefixe).unwrap());
    Ok(bs58::encode(hex::decode(prefixe).expect("Invalid Hex String")).into_string())
}

pub fn p2pkh_scriptsig_to_address(scriptSig: String) -> Result<String, std::io::Error> {
    let script_reversed = translate(&scriptSig).unwrap();

    let push_data_1 = usize::from_str_radix(&script_reversed[0..2], 16).unwrap();
    let push_data_2_start: usize = (push_data_1 + 1) * 2;
    let push_data_2_end: usize = (push_data_1 + 2) * 2;
    let push_data_2 =
        usize::from_str_radix(&script_reversed[push_data_2_start..push_data_2_end], 16).unwrap();
    let index_pubkey_start: usize = push_data_2_start + 2;
    let index_pubkey_end: usize = index_pubkey_start + (push_data_2 * 2);
    let pubkey: String = script_reversed[index_pubkey_start..index_pubkey_end].to_string();

    Ok(pubkey_to_address(pubkey).unwrap())
}

pub fn p2ms_scriptpk_to_pubkey(script_pub_key: &String) -> Result<Vec<String>, std::io::Error> {
    let mut result_tab: Vec<String> = Vec::new();
    let script_reversed = translate(script_pub_key).unwrap();
    let mut push_data_str = &script_reversed[2..4];
    let mut push_data: usize = usize::from_str_radix(push_data_str, 16).unwrap();
    let taille = script_reversed.len();
    let mut start: usize = 4;
    let mut end: usize = start + (2 * push_data);

    loop {
        let pubkey: String = script_reversed[start..end].to_string();
        result_tab.push(pubkey);

        if (end + 4) == taille {
            break;
        } else {
            push_data_str = &script_reversed[end..end + 2];
        }
        push_data = usize::from_str_radix(push_data_str, 16).unwrap();
        start = end + 2;
        end = start + (2 * push_data);
    }
    Ok(result_tab)
}

pub fn p2ms_scriptpk_to_address(script_pub_key: &String) -> Result<Vec<String>, std::io::Error> {
    let tab: Vec<String> = p2ms_scriptpk_to_pubkey(script_pub_key).unwrap();
    let mut result_tab: Vec<String> = Vec::new();

    for i in 0..tab.len() {
        result_tab.push(pubkey_to_address(tab[i].clone()).unwrap());
    }

    Ok(result_tab)
}

pub fn p2sh_scriptpk_to_address(script_pub_key: &String) -> Result<String, std::io::Error> {
    let mut script_reversed = translate(&script_pub_key).unwrap();
    script_reversed.remove(0);
    script_reversed.remove(0);
    script_reversed.remove(0);
    script_reversed.remove(0);
    script_reversed.remove(script_reversed.len() - 1);
    script_reversed.remove(script_reversed.len() - 1);
    let mut prefixe: String = String::from("05");
    prefixe.push_str(&script_reversed);
    prefixe.push_str(&checksum(&prefixe).unwrap());

    Ok(bs58::encode(hex::decode(prefixe).expect("Invalid Hex String")).into_string())
}

pub fn p2sh_scriptsig_to_hash(script: &String) -> Result<String, std::io::Error> {
    let script_reversed = translate(&script).unwrap();
    let push_data_1 = usize::from_str_radix(&script_reversed[2..4], 16).unwrap();
    let push_data_2_start: usize = (push_data_1 + 2) * 2;
    let push_data_2_end: usize = (push_data_1 + 3) * 2;

    let push_data_2 =
        usize::from_str_radix(&script_reversed[push_data_2_start..push_data_2_end], 16).unwrap();
    let index_redeem_start: usize = push_data_2_start + 2;
    let index_redeem_end: usize = index_redeem_start + (push_data_2 * 2);
    let redeem: String = script_reversed[index_redeem_start..index_redeem_end].to_string();

    let mut hasher_256 = Sha256::new();
    hasher_256.update(hex::decode(redeem).expect("Invalid Hex String"));
    let hash256_res = format!("{:x}", hasher_256.finalize());

    let mut hasher_160 = Ripemd160::new();
    hasher_160.update(hex::decode(hash256_res).expect("Invalid Hex String"));
    let ripemd160_res: String = format!("{:x}", hasher_160.finalize());

    Ok(ripemd160_res)
}

pub fn p2sh_scriptsig_to_address(script: &String) -> Result<Vec<String>, std::io::Error> {
    let script_reversed = translate(&script).unwrap();
    let push_data_1 = usize::from_str_radix(&script_reversed[2..4], 16).unwrap();
    let push_data_2_start: usize = (push_data_1 + 2) * 2;
    let push_data_2_end: usize = (push_data_1 + 3) * 2;

    let push_data_2 =
        usize::from_str_radix(&script_reversed[push_data_2_start..push_data_2_end], 16).unwrap();
    let index_redeem_start: usize = push_data_2_start + 2;
    let index_redeem_end: usize = index_redeem_start + (push_data_2 * 2);
    let redeem: String = script_reversed[index_redeem_start..index_redeem_end].to_string();

    let mut result_tab: Vec<String> = Vec::new();
    let mut push_data_str = &redeem[2..4];
    let mut push_data: usize = usize::from_str_radix(push_data_str, 16).unwrap();
    let taille = redeem.len();
    let mut start: usize = 4;
    let mut end: usize = start + (2 * push_data);

    loop {
        let pubkey: String = redeem[start..end].to_string();
        result_tab.push(pubkey_to_address(pubkey).unwrap());

        if (end + 4) == taille {
            break;
        } else {
            push_data_str = &redeem[end..end + 2];
        }
        push_data = usize::from_str_radix(push_data_str, 16).unwrap();
        start = end + 2;
        end = start + (2 * push_data);
    }
    Ok(result_tab)
}

pub fn generic_scriptpk_to_address(script_pub_key: &String) -> Result<Vec<String>, std::io::Error> {
    let re_op_return = Regex::new(r"^6a").unwrap();
    let re_hash160 = Regex::new(r"^a9").unwrap();
    let re_op_1 = Regex::new(r"^51").unwrap();
    let re_op_dup_hash160 = Regex::new(r"^76a9").unwrap();
    let mut tab: Vec<String> = Vec::new();

    let script_pub_key_bon = translate(&script_pub_key.clone()).unwrap();

    if !re_op_return.is_match(script_pub_key_bon.as_str()) {
        // !NULL DATA
        if re_op_dup_hash160.is_match(script_pub_key_bon.as_str()) {
            //PAY TO PUBKEY HASH
            let address = p2pkh_scriptpk_to_address(&script_pub_key).unwrap();
            tab.push(address);
        } else if re_op_1.is_match(script_pub_key_bon.as_str()) {
            //PAY TO MULTISIG
            let addresses = p2ms_scriptpk_to_address(&script_pub_key).unwrap();
            tab = addresses;
        } else if re_hash160.is_match(script_pub_key_bon.as_str()) {
            //PAY TO SCRIPT HASH
            let address = p2sh_scriptpk_to_address(&script_pub_key).unwrap();
            tab.push(address);
        } else {
            //PAY TO PUBKEY
            let address = p2pk_scriptpk_to_address(&script_pub_key).unwrap();
            tab.push(address);
        }
    }

    Ok(tab)
}

pub fn hashmap_address(blocks: &Vec<Block>) -> HashMap<String, String> {
    let mut total = 0;
    // let mut cpt = 0;
    let mut hashmap = HashMap::new();

    let re_op_return = Regex::new(r"^6a").unwrap();
    let re_hash160 = Regex::new(r"^a9").unwrap();
    let re_op_1 = Regex::new(r"^51").unwrap();
    let re_op_dup_hash160 = Regex::new(r"^76a9").unwrap();

    let mut p2pk = 0;
    let mut p2sh = 0;
    let mut p2ms = 0;
    let mut p2pkh = 0;

    for block in blocks {
        let transactions = block.transaction();
        for transaction in transactions {
            let outputs = transaction.outputs();
            // let inputs = transaction.get_inputs();
            for output in outputs {
                let script_pub_key = output.script_pub_key_to_string();
                let script_pub_key_bon = translate(&script_pub_key.clone()).unwrap();

                if !re_op_return.is_match(script_pub_key_bon.as_str()) {
                    // !NULL DATA

                    if re_op_dup_hash160.is_match(script_pub_key_bon.as_str()) {
                        //PAY TO PUBKEY HASH
                        let address = p2pkh_scriptpk_to_address(&script_pub_key.clone()).unwrap();
                        if !hashmap.contains_key(&address.clone()) {
                            hashmap
                                .insert(address.clone(), address_to_hash(address.clone()).unwrap());
                            //println!("P2PKH {}", address);
                            p2pkh += 1;
                        }
                    } else if re_op_1.is_match(script_pub_key_bon.as_str()) {
                        //PAY TO MULTISIG
                        let addresses = p2ms_scriptpk_to_address(&script_pub_key.clone()).unwrap();
                        for i in addresses {
                            if !hashmap.contains_key(&i.clone()) {
                                hashmap.insert(i.clone(), address_to_hash(i.clone()).unwrap());
                                //println!("P2MS {}", i);
                                p2ms += 1;
                            }
                        }
                    } else if re_hash160.is_match(script_pub_key_bon.as_str()) {
                        //PAY TO SCRIPT HASH
                        let address = p2sh_scriptpk_to_address(&script_pub_key.clone()).unwrap();
                        if !hashmap.contains_key(&address.clone()) {
                            hashmap
                                .insert(address.clone(), address_to_hash(address.clone()).unwrap());
                            //println!("P2SH {}", address);
                            p2sh += 1;
                        }
                    } else {
                        //PAY TO PUBKEY
                        let address = p2pk_scriptpk_to_address(&script_pub_key.clone()).unwrap();
                        if !hashmap.contains_key(&address.clone()) {
                            hashmap
                                .insert(address.clone(), address_to_hash(address.clone()).unwrap());
                            //println!("P2PK {}", address);
                            p2pk += 1;
                        }
                    }
                }

                total += 1;

                if total % 10000 == 0 {
                    println!("{}", total);
                }
            }
        }
    }
    println!("Répartition scripts");
    println!("P2PK {}", p2pk);
    println!("P2PKH {}", p2pkh);
    println!("P2SH {}", p2sh);
    println!("P2MS {}", p2ms);
    println!("TOTAL {}", total);

    hashmap
}

pub fn bloc_addresses(block: &Block) -> Vec<String> {
    let mut tab: Vec<String> = Vec::new();
    let mut hashmap = HashMap::new();

    let re_op_return = Regex::new(r"^6a").unwrap();
    let re_hash160 = Regex::new(r"^a9").unwrap();
    let re_op_1 = Regex::new(r"^51").unwrap();
    let re_op_dup_hash160 = Regex::new(r"^76a9").unwrap();

    let transactions = block.transaction();

    for transaction in transactions {
        let outputs = transaction.outputs();
        for output in outputs {
            let script_pub_key = output.script_pub_key_to_string();
            let script_pub_key_bon = translate(&script_pub_key.clone()).unwrap();

            if !re_op_return.is_match(script_pub_key_bon.as_str()) {
                // !NULL DATA
                if re_op_dup_hash160.is_match(script_pub_key_bon.as_str()) {
                    //PAY TO PUBKEY HASH
                    let address = p2pkh_scriptpk_to_address(&script_pub_key.clone()).unwrap();
                    if !hashmap.contains_key(&address.clone()) {
                        hashmap.insert(address.clone(), address_to_hash(address.clone()).unwrap());
                    }
                } else if re_op_1.is_match(script_pub_key_bon.as_str()) {
                    //PAY TO MULTISIG
                    let addresses = p2ms_scriptpk_to_address(&script_pub_key.clone()).unwrap();
                    for i in addresses {
                        if !hashmap.contains_key(&i.clone()) {
                            hashmap.insert(i.clone(), address_to_hash(i.clone()).unwrap());
                        }
                    }
                } else if re_hash160.is_match(script_pub_key_bon.as_str()) {
                    //PAY TO SCRIPT HASH
                    let address = p2sh_scriptpk_to_address(&script_pub_key.clone()).unwrap();
                    if !hashmap.contains_key(&address.clone()) {
                        hashmap.insert(address.clone(), address_to_hash(address.clone()).unwrap());
                    }
                } else {
                    //PAY TO PUBKEY
                    let address = p2pk_scriptpk_to_address(&script_pub_key.clone()).unwrap();
                    if !hashmap.contains_key(&address.clone()) {
                        hashmap.insert(address.clone(), address_to_hash(address.clone()).unwrap());
                    }
                }
            }
        }
    }

    for (add, _hash) in hashmap {
        tab.push(add);
    }

    tab
}
