#![allow(non_snake_case)]
#![allow(dead_code)]

mod Calculs;
mod acces;
mod structure;
mod tests;
mod traitements;
extern crate strum_macros;

use crate::structure::block_struct::Block;
use crate::traitements::interpretations_functions;
use crate::traitements::interpretations_functions::run_write;
use crate::traitements::tris::sort_by_blocks_dates;

use std::{
    env,
    fs,
    fs::ReadDir,
    io,
    path::Path,
};

use crossterm::{
    event::EnableMouseCapture,
    execute,
    terminal::{enable_raw_mode, EnterAlternateScreen, ScrollDown, ScrollUp},
};

use tui::{backend::CrosstermBackend, Terminal};

fn main() -> Result<(), std::io::Error> {
    let argv: Vec<String> = env::args().collect();
    let argc = argv.len();

    let mut files: Vec<String> = Vec::new();
    let mut out_files: Vec<String> = Vec::new();
    let mut files_verif: Vec<String> = Vec::new();

    let mut it_index: usize = 0;
    let mut leave: bool = false;

    let mut verif: Vec<(String, bool)> = Vec::new();

    if argc == 1
        || (argv[1] == "help" || argv[1] == "h" || argv[1] == "a" || argv[1] == "aide")
        || (argv[1] != "o"
            && argv[1] != "out"
            && argv[1] != "i"
            && argv[1] != "in"
            && argv[1] != "io"
            && argv[1] != "inout"
            && argv[1] != "d"
            && argv[1] != "defaut"
            && argv[1] != "default")
    {
        println!("\nUtilisation: {{cargo run --release, [chemin_bin_cargo_install]}} [choix] [dossier] [dossier]");
        println!("Permet la lecture de fichiers .dat issu de la Blockchain Bitcoin \n");
        println!("  d, defaut, default                         mode par défaut (les fichiers à lire doivent être mis dans ./ins/ et seront exportés dans ./outs/) \n");
        println!("  o, out [chemin_sortie]                     permet de choisir le dossier d'exportation");
        println!("  i, in [chemin_entrée]                      permet de choisir le dossier contenant les fichiers à lire");
        println!("  io, inout [chemin_entrée] [chemin_sortie]  permet de choisir le dossier contenant les fichiers à lire et le dossier où exporter les résultats\n");
        println!("Exemples :");
        println!("cargo run --release i ./ins/                         lit les fichiers .dat dans le dossier ./ins/");
        println!("/home/username/.cargo/bin/projet i ./ins/            (Arch Linux cargo install) lit les fichiers .dat dans le dossier ./ins/");
        println!("C:/Users/[username]/.cargo/bin/projet i ./ins/       (Windows cargo install) lit les fichiers .dat dans le dossier ./ins/");
        println!("$HOME/.cargo/bin/projet i ./ins/                     (Ubuntu cargo install) lit les fichiers .dat dans le dossier ./ins/");
        println!("cargo run --release o ./outs/                        écrit les exportations en .json/.txt dans le dossier ./outs/");
        println!("cargo run --release io ./ins/ ./outs/                lit les fichiers .dat dans ./ins et écrit en .json /.txt dans ./outs/\n");
        return Ok(());
    } else if (argv[1] == "-d"
        || argv[1] == "d"
        || argv[1] == "--defaut"
        || argv[1] == "--default"
        || argv[1] == "defaut"
        || argv[1] == "default")
        && argc == 2
    {
        // mode par defaut

        let exist_ins = Path::new("./ins/").is_dir();

        if exist_ins == false {
            fs::create_dir("./ins/")?
        }

        let exists_outs = Path::new("./outs/").is_dir();

        if exists_outs == false {
            fs::create_dir("./outs/")?
        }

        let paths: ReadDir = fs::read_dir("./ins/").unwrap();

        for path in paths {
            let entry = path.unwrap();

            let entry_path = entry.path();

            if entry_path.extension().unwrap() == "dat" {
                let file_name = entry_path.file_name().unwrap();

                let file_name_as_str = file_name.to_str().unwrap();
                let file_name_str_sorties = entry_path.file_stem().unwrap().to_str().unwrap();

                let mut file_name_as_string = String::from("./ins/");

                let mut file_name_as_string_sorties = String::from("./outs/");

                file_name_as_string.push_str(file_name_as_str);
                file_name_as_string_sorties.push_str(file_name_str_sorties);

                files.push(file_name_as_string);
                out_files.push(file_name_as_string_sorties);
            }
        }
    } else if argc == 3 && (argv[1] == "o" || argv[1] == "out") {

        let exist_ins = Path::new("./ins/").is_dir();

        if exist_ins == false {
            fs::create_dir("./ins/")?
        }

        let paths: ReadDir = fs::read_dir("./ins/").unwrap();

        for path in paths {
            let entry = path.unwrap();

            let entry_path = entry.path();

            if entry_path.extension().unwrap() == "dat" {
                let file_name = entry_path.file_name().unwrap();

                let file_name_as_str = file_name.to_str().unwrap();
                let file_name_str_sorties = entry_path.file_stem().unwrap().to_str().unwrap();

                let mut file_name_as_string = String::from("./ins/");

                if Path::new(&argv[2].clone()).is_dir() == false {
                    return Err(io::Error::new(
                        io::ErrorKind::Other,
                        "Le dossier de sortie n est pas un dossier !",
                    ));
                }

                let mut file_name_as_string_sorties = String::from(argv[2].clone());

                file_name_as_string.push_str(file_name_as_str);
                file_name_as_string_sorties.push_str(file_name_str_sorties);

                files.push(file_name_as_string);
                out_files.push(file_name_as_string_sorties);
            }
        }
    } else if argc == 3 && (argv[1] == "i" || argv[1] == "in") {
        if Path::new(&argv[2].clone()).is_dir() == false {
            return Err(io::Error::new(
                io::ErrorKind::Other,
                "Le dossier d'entrée n est pas un dossier !",
            ));
        }

        let exists_outs = Path::new("./outs/").is_dir();

        if exists_outs == false {
            fs::create_dir("./outs/")?
        }


        let paths: ReadDir = fs::read_dir(argv[2].clone()).unwrap();

        for path in paths {
            let entry = path.unwrap();

            let entry_path = entry.path();

            if entry_path.extension().unwrap() == "dat" {
                let file_name = entry_path.file_name().unwrap();

                let file_name_as_str = file_name.to_str().unwrap();
                let file_name_str_sorties = entry_path.file_stem().unwrap().to_str().unwrap();

                let mut file_name_as_string = String::from(argv[2].clone());

                let mut file_name_as_string_sorties = String::from("./outs/");

                file_name_as_string.push_str(file_name_as_str);
                file_name_as_string_sorties.push_str(file_name_str_sorties);

                files.push(file_name_as_string);
                out_files.push(file_name_as_string_sorties);
            }
        }
    } else if argc == 4 && (argv[1] == "io" || argv[1] == "inout") {
        if Path::new(&argv[2].clone()).is_dir() == false {
            return Err(io::Error::new(
                io::ErrorKind::Other,
                "Le dossier d'entrée n est pas un dossier !",
            ));
        }

        let paths: ReadDir = fs::read_dir(argv[2].clone()).unwrap();

        for path in paths {
            let entry = path.unwrap();

            let entry_path = entry.path();

            if entry_path.extension().unwrap() == "dat" {
                let file_name = entry_path.file_name().unwrap();

                let file_name_as_str = file_name.to_str().unwrap();
                let file_name_str_sorties = entry_path.file_stem().unwrap().to_str().unwrap();

                let mut file_name_as_string = String::from(argv[2].clone());

                if Path::new(&argv[3].clone()).is_dir() == false {
                    return Err(io::Error::new(
                        io::ErrorKind::Other,
                        "Le dossier de sortie n est pas un dossier !",
                    ));
                }

                let mut file_name_as_string_sorties = String::from(argv[3].clone());

                file_name_as_string.push_str(file_name_as_str);
                file_name_as_string_sorties.push_str(file_name_str_sorties);

                files.push(file_name_as_string);
                out_files.push(file_name_as_string_sorties);
            }
        }
    }

    let index = files.len();
    out_files.sort();
    files.sort();

    if files.len() == 0 {
        println!("Le dossier d'entrée ne contient pas de fichiers .dat");
    }

    while it_index < index && leave == false {
        files_verif.push(files[it_index].clone());

        let mut write: bool = false;

        let mut blks: Vec<Block> = Vec::new();

        acces::lecture_frac::read_file_threads(&files[it_index], &mut blks).unwrap();

        sort_by_blocks_dates(&mut blks);

        let v = interpretations_functions::function_test(
            &blks,
            &files,
            &files_verif,
            &mut it_index,
            &mut leave,
            &mut write,
            &mut verif,
        );

        if v.is_err() {
            return Err(io::Error::new(
                io::ErrorKind::Other,
                "Probleme dans la fonction d affichage",
            ));
        }

        if write {
            let mut json: bool = false;
            let mut txt: bool = false;
            run_write(&it_index, &mut json, &mut txt, &mut out_files)?;

            enable_raw_mode()?;
            let mut stdout = io::stdout();

            execute!(
                stdout,
                EnterAlternateScreen,
                EnableMouseCapture,
                ScrollUp(1000),
                ScrollDown(1000)
            )?;

            let backend = CrosstermBackend::new(stdout);
            let mut terminal = Terminal::new(backend)?;

            terminal.clear()?;

            if txt {
                let _ec = acces::lecture_frac::write_txt(
                    &out_files[it_index - 1],
                    &blks,
                    &mut terminal,
                );
            } else if json {
                let _ec = acces::lecture_frac::write_json(
                    &out_files[it_index - 1],
                    &blks,
                    &mut terminal,
                );
            }
        }
    }

    return Ok(());
}
