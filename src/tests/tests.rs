#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn tests_parseur() {
        // initialisation
        let metadata_file = fs::metadata(&String::from("./ins/blk00000.dat"));
        let metadata_file = match metadata_file {
            Ok(metadata_file) => metadata_file,
            Err(error) => panic!("Problem opening the file: {:?}", error),
        };
        let mut blocks: Vec<Block> = Vec::new();
        lecture_frac::lecture_fichier(
            &String::from("./ins/blk00000.dat"),
            &mut blocks,
            metadata_file.len(),
        )
        .unwrap();

        // vérification de la version du bloc 0
        let version_extraite: i32 = blocks[0].get_block_header().get_version();
        let version_reelle: i32 = 1;
        assert_eq!(version_extraite, version_reelle);

        // vérification de la version du bloc 2770
        let version_extraite: i32 = blocks[2770].get_block_header().get_version();
        let version_reelle: i32 = 1;
        assert_eq!(version_extraite, version_reelle);

        // vérification de la racine de merkle du bloc 0
        let racine_merkle_extraite: String = blocks[0].get_block_header().merkle_root_to_string();
        let racine_merkle_reelle: String =
            String::from("4a5e1e4baab89f3a32518a88c31bc87f618f76673e2cc77ab2127b7afdeda33b");
        assert_eq!(racine_merkle_extraite, racine_merkle_reelle);

        // vérification de la racine de merkle du bloc 2770
        let racine_merkle_extraite: String =
            blocks[2770].get_block_header().merkle_root_to_string();
        let racine_merkle_reelle: String =
            String::from("cc08a7ce7fb75c23fd6518c2e399fc92b693f7fe6bce4a344d3f102a0695fe22");
        assert_eq!(racine_merkle_extraite, racine_merkle_reelle);

        // vérification de la date du bloc 0
        let date_extraite: DateTime<Utc> = DateTime::from_utc(
            NaiveDateTime::from_timestamp((*blocks[0].get_block_header().get_time()) as i64, 0),
            Utc,
        );
        let date_reelle: DateTime<Utc> = DateTime::<Utc>::from_utc(
            chrono::NaiveDate::parse_from_str("03/01/2009", "%d/%m/%Y")
                .unwrap()
                .and_hms(0, 0, 0),
            Utc,
        );
        let difference_en_jours: i64 =
            (date_extraite.signed_duration_since(date_reelle)).num_days();
        assert!(difference_en_jours == 0);

        // vérification de la date du bloc 2770
        let date_extraite: DateTime<Utc> = DateTime::from_utc(
            NaiveDateTime::from_timestamp((*blocks[2770].get_block_header().get_time()) as i64, 0),
            Utc,
        );
        let date_reelle: DateTime<Utc> = DateTime::<Utc>::from_utc(
            chrono::NaiveDate::parse_from_str("03/02/2009", "%d/%m/%Y")
                .unwrap()
                .and_hms(0, 0, 0),
            Utc,
        );
        let difference_en_jours: i64 =
            (date_extraite.signed_duration_since(date_reelle)).num_days();
        assert!(difference_en_jours == 0);

        // vérification de la cible du bloc 0
        let cible_extraite: u32 = blocks[0].get_block_header().get_bits();
        let cible_reelle: u32 = 486604799;
        assert_eq!(cible_extraite, cible_reelle);

        // vérification de la cible du bloc 2770
        let cible_extraite: u32 = blocks[2770].get_block_header().get_bits();
        let cible_reelle: u32 = 486604799;
        assert_eq!(cible_extraite, cible_reelle);

        // vérification du nonce du bloc 0
        let nonce_extrait: u32 = blocks[0].get_block_header().get_nonce();
        let nonce_reel: u32 = 2083236893;
        assert_eq!(nonce_extrait, nonce_reel);

        // vérification du nonce du bloc 2770
        let nonce_extrait: u32 = blocks[2770].get_block_header().get_nonce();
        let nonce_reel: u32 = 10371590;
        assert_eq!(nonce_extrait, nonce_reel);

        // vérification du nombre de transactions du bloc 0
        let nombre_transactions_extrait: i32 = *blocks[0].tx_count();
        let nombre_transactions_reel: i32 = 1;
        assert_eq!(nombre_transactions_extrait, nombre_transactions_reel);

        // vérification du nombre de transactions du bloc 2770
        let nombre_transactions_extrait: i32 = *blocks[2770].tx_count();
        let nombre_transactions_reel: i32 = 6;
        assert_eq!(nombre_transactions_extrait, nombre_transactions_reel);

        // vérification du calcul de la somme moyenne transférée du bloc 0
        let valeur_moyenne_calculee: f64 = blocks[0].average_value();
        let valeur_moyenne_reelle: f64 = 50.00000000;
        let difference_en_satoshis: f64 = valeur_moyenne_calculee - valeur_moyenne_reelle;
        assert!(difference_en_satoshis > -1. && difference_en_satoshis < 1.);

        // vérification du calcul de la somme moyenne transférée du bloc 2770
        let valeur_moyenne_calculee: f64 = blocks[2770].average_value();
        let valeur_moyenne_reelle: f64 = 25.88727273;
        let difference_en_satoshis: f64 = valeur_moyenne_calculee - valeur_moyenne_reelle;
        assert!(difference_en_satoshis > -1. && difference_en_satoshis < 1.);

        // vérification de la recherche de la plus grande somme transférée du fichier 0
        let plus_grande_transaction = chercherPlusGrandeSomme(&blocks);
        let script_pub_key_extrait: String = plus_grande_transaction.get_script_pub_key();
        let script_pub_key_reel: String =
            String::from("ac8857797fcb518688d90b60f5d1a63fc70fdb40c16814a976");
        let somme_extraite: i64 = plus_grande_transaction.get_value();
        let somme_reelle: i64 = 40000000000000;
        assert_eq!(script_pub_key_extrait, script_pub_key_reel);
        assert_eq!(somme_extraite, somme_reelle);

        // vérification du nombre de blocs extraits du fichier 0
        let nombre_blocs_extraits: i32 = blocks.len() as i32;
        let nombre_blocs_reel: i32 = 120004;
        assert_eq!(nombre_blocs_extraits, nombre_blocs_reel);
    }

    #[test]
    fn test_bits_vers_cible_1() {
        // vérification de la conversion des bits vers la cible
        let cible_calculee: [u8; 32] = bits_vers_target(486604799);
        let cible_reelle: [u8; 32] = [
            0, 0, 0, 0, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0,
        ];
        let mut egalite: bool = true;
        for i in 0..32 {
            if cible_calculee[i] != cible_reelle[i] {
                egalite = false;
            }
        }
        assert!(egalite);
    }

    #[test]
    fn test_bits_vers_cible_2() {
        // vérification de la conversion des bits vers la cible
        let cible_calculee: [u8; 32] = bits_vers_target(0x180696f4);
        let cible_reelle: [u8; 32] = [
            0, 0, 0, 0, 0, 0, 0, 0, 6, 150, 244, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0,
        ];
        let mut egalite: bool = true;
        for i in 0..32 {
            if cible_calculee[i] != cible_reelle[i] {
                egalite = false;
            }
        }
        assert!(egalite);
    }

    #[test]
    #[should_panic]
    fn test_bits_vers_cible_3() {
        // vérification de la conversion des bits vers la cible (erreur)
        let cible_calculee: [u8; 32] = bits_vers_target(0x180696f4);
        let cible_reelle: [u8; 32] = [
            0, 0, 0, 0, 0, 0, 0, 0, 7, 150, 244, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0,
        ];
        let mut egalite: bool = true;
        for i in 0..32 {
            if cible_calculee[i] != cible_reelle[i] {
                egalite = false;
            }
        }
        assert!(egalite);
    }

    #[test]
    fn test_satoshi_vers_bitcoin_1() {
        // vérification de la conversion satoshi vers bitcoin
        let bitcoin_calcule: i64 = satoshi_vers_bitcoin(5000000000);
        let bitcoin_reel: i64 = 50;
        assert_eq!(bitcoin_calcule, bitcoin_reel);
    }

    #[test]
    #[should_panic]
    fn test_satoshi_vers_bitcoin_2() {
        // vérification de la conversion satoshi vers bitcoin (erreur)
        let bitcoin_calcule: i64 = satoshi_vers_bitcoin(6000000000);
        let bitcoin_reel: i64 = 50;
        assert_eq!(bitcoin_calcule, bitcoin_reel);
    }

    #[test]
    fn test_lecture_nombre_blocs() {
        // vérification de la fonction de comptage du nombre de blocs du fichier 0
        let nombre_blocs_calcul =
            lecture_frac::nombreBlocksFichierSecurise(&String::from("./ins/blk00000.dat"));
        let nombre_blocs_calcul: i32 = match nombre_blocs_calcul {
            Ok(nombre_blocs_calcul) => nombre_blocs_calcul,
            Err(error) => panic!("Problem accessing blocks number: {:?}", error),
        };
        let nombre_blocs_reel: i32 = 120004;
        assert_eq!(nombre_blocs_calcul, nombre_blocs_reel);
    }
}
