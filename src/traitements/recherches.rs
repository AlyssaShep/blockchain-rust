#![allow(non_snake_case)]

use crate::structure::{
    block_struct::Block,
    output_struct::Output,
    transaction_struct::Transaction,
};

use crate::traitements::{
    conversions,
    conversions::satoshi_to_bitcoin,
    tris::sort_by_blocks_dates
};

use std::fmt;

use std::io::Write as OtherWrite;

use chrono::prelude::*;

pub struct HighestTransaction<'a> {
    block: &'a Block,
    transaction: &'a Transaction,
    output: &'a Output,
    value: f64,
}

impl fmt::Debug for HighestTransaction<'_> {
    fn fmt<'a>(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[HighestTransaction : \n  value {:?} \n]", self.value)
    }
}

impl HighestTransaction<'_> {
    pub fn new<'a>(
        block: &'a Block,
        transaction: &'a Transaction,
        output: &'a Output,
        value: f64,
    ) -> HighestTransaction<'a> {
        return HighestTransaction {
            block,
            transaction,
            output,
            value,
        };
    }

    pub fn get_block(&self) -> &Block {
        &self.block
    }

    pub fn get_value(&self) -> f64 {
        self.value
    }

    pub fn get_script_pub_key(&self) -> String {
        self.output.script_pub_key_to_string()
    }
}

pub fn search_for_highest_value(tab_blocks: &Vec<Block>) -> HighestTransaction {

    let mut highest_value: i64 = 0;
    let mut id_highest_value: usize = 0;
    let mut id_transaction: usize = 0;
    let mut id_block: usize = 0;

    for i in 0..tab_blocks.len() {
        for j in 0..tab_blocks[i].transaction().len() {
            for k in 0..tab_blocks[i].transaction()[j].outputs().len() {
                if tab_blocks[i].transaction()[j].outputs()[k].value() > &highest_value {
                    highest_value = *tab_blocks[i].transaction()[j].outputs()[k].value();
                    id_highest_value = k;
                    id_transaction = j;
                    id_block = i;
                }
            }
        }
    }

    return HighestTransaction::new(
        &tab_blocks[id_block],
        &tab_blocks[id_block].transaction()[id_transaction],
        &tab_blocks[id_block].transaction()[id_transaction].outputs()[id_highest_value],
        satoshi_to_bitcoin(highest_value),
    );
}

pub fn search_for_highest_value_per_block<'a>(
    tab_blocks: &'a Vec<Block>,
    highest_value: &'a mut f64,
 ) -> Vec<HighestTransaction<'a>> {

    let mut highest_value_per_block: Vec<HighestTransaction> =
        Vec::with_capacity(tab_blocks.len());

    let mut the_highest_value: i64 = 0;

    for i in 0..tab_blocks.len() {
        let mut highest_value: i64 = 0;
        let mut id_highest_value: usize = 0;
        let mut id_transaction: usize = 0;
        let mut id_block: usize = 0;

        for j in 0..tab_blocks[i].transaction().len() {
            for k in 0..tab_blocks[i].transaction()[j].outputs().len() {
                if tab_blocks[i].transaction()[j].outputs()[k].value() > &highest_value {
                    highest_value = *tab_blocks[i].transaction()[j].outputs()[k].value();
                    id_highest_value = k;
                    id_transaction = j;
                    id_block = i;
                }

                if tab_blocks[i].transaction()[j].outputs()[k].value() > &the_highest_value {
                    the_highest_value = *tab_blocks[i].transaction()[j].outputs()[k].value();
                }
            }
        }

        highest_value_per_block.push(HighestTransaction::new(
            &tab_blocks[id_block],
            &tab_blocks[id_block].transaction()[id_transaction],
            &tab_blocks[id_block].transaction()[id_transaction].outputs()
                [id_highest_value],
                satoshi_to_bitcoin(highest_value),
        ));
    }

    *highest_value = satoshi_to_bitcoin(the_highest_value) as f64;

    return highest_value_per_block;
}

pub fn dayly_bitcoin(time: u32, tab_blocks: &Vec<Block>) -> f64 {

    let mut blk: Vec<Block> = Vec::new();
    for it in tab_blocks {
        blk.push(it.clone());
    }

    sort_by_blocks_dates(&mut blk);

    let mut i: usize = 0;
    let mut amount: i64 = 0;

    while (i < tab_blocks.len())
        && (conversions::higher_equal_date(
            conversions::timestamp_to_time(time as i64),
            conversions::timestamp_to_time(
                *tab_blocks[i].get_block_header().get_time() as i64
            ),
        ))
    {

        if conversions::equal_date(
            conversions::timestamp_to_time(time as i64),
            conversions::timestamp_to_time(
                *tab_blocks[i].get_block_header().get_time() as i64
            ),
        ) {
            for j in 0..tab_blocks[i].transaction().len() {
                for k in 0..tab_blocks[i].transaction()[j].outputs().len() {
                    amount += *tab_blocks[i].transaction()[j].outputs()[k].value() as i64;
                }
            }
        }

        i = i + 1;
    }

    let bitcoin_amount = conversions::satoshi_to_bitcoin(amount);

    return bitcoin_amount;
}

pub fn dayly_bitcoin_24h(time: u32, tab_blocks: &Vec<Block>) -> f64 {
    println!("Tri des blocs...");

    println!("Fin du tri des blocs.");

    println!("Recherche de tous les blocs du jour demandé...");

    let mut i: usize = 0;
    let mut amount: i64 = 0;

    while (i < tab_blocks.len()) && (time >= *tab_blocks[i].get_block_header().get_time()) {
        println!("taille : {}", tab_blocks.len());
        println!("Premier timestamp : {}", time);
        println!(
            "Second timestamp : {}",
            *tab_blocks[0].get_block_header().get_time()
        );

        let difference: i64 =
            (time as i64 - *tab_blocks[i].get_block_header().get_time() as i64).abs();
        println!("difference : {}", difference);
        if difference <= 86400 {
            for j in 0..tab_blocks[i].transaction().len() {
                for k in 0..tab_blocks[i].transaction()[j].outputs().len() {
                    amount += *tab_blocks[i].transaction()[j].outputs()[k].value() as i64;
                    println!(
                        "value : {}",
                        *tab_blocks[i].transaction()[j].outputs()[k].value() as i64
                    );
                }
            }
        }

        i = i + 1;
    }

    let bitcoin_amount = conversions::satoshi_to_bitcoin(amount);

    return bitcoin_amount;
}

// s'appelle sur des blocs triés par date
pub fn transactions_evolution_csv(blocks: Vec<&Block>, intervale_in_days: u32) {
    let mut name_file = std::fs::File::create("transactions.csv").unwrap();
    write!(name_file, "Période;Transactions\n").unwrap();

    let intervale: u32 = 60 * 60 * 24 * intervale_in_days;

    let mut horaire_precedent: u32 = *blocks[0].get_block_header().get_time();
    let mut transactions: u128 = *blocks[0].tx_count() as u128;

    let mut horaire_courant: u32;
    let mut horaire_naif: NaiveDateTime;
    let mut horaire_affichage: String;

    for block in blocks {
        horaire_courant = *block.get_block_header().get_time();
        if horaire_courant >= horaire_precedent && (horaire_courant - horaire_precedent) > intervale
        {
            horaire_naif = NaiveDateTime::from_timestamp(horaire_precedent as i64, 0);
            horaire_affichage = horaire_naif.format("%Y-%m-%d").to_string();
            horaire_precedent = horaire_courant;
            write!(name_file, "{};{}\n", horaire_affichage, transactions).unwrap();
            transactions = *block.tx_count() as u128;
        } else if horaire_courant >= horaire_precedent {
            transactions += *block.tx_count() as u128;
        }
    }
}
