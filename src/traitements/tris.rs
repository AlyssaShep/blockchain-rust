#![allow(non_snake_case)]

use crate::structure::block_struct::Block;
use crate::structure::transaction_struct::Transaction;

pub fn sort_by_transactions(tab_blocks: &Vec<Block>) -> Vec<&Transaction> {
    let mut tab_res: Vec<&Transaction> = Vec::new();

    for i in 0..tab_blocks.len() {
        for j in 0..tab_blocks[i].transaction().len() {
            tab_res.push(&tab_blocks[i].transaction()[j]);
        }
    }

    tab_res.sort_by(|a, b| b.all_values_outputs().cmp(&a.all_values_outputs()));

    return tab_res;
}

pub fn sort_by_blocks_dates(blocks: &mut Vec<Block>) {
    blocks.sort_by(|a, b| {
        a.get_block_header()
            .get_time()
            .partial_cmp(b.get_block_header().get_time())
            .unwrap()
    });
}
