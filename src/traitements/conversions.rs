#![allow(non_snake_case)]

use chrono::prelude::*;

pub fn binary_to_decimal(binary_word: &String) -> Result<i32, std::io::Error> {
    if binary_word.is_empty() {
        return Err(std::io::Error::new(
            std::io::ErrorKind::InvalidInput,
            "ERREUR : La chaîne est vide.",
        ));
    }

    for c in binary_word.chars() {
        if !c.is_digit(10) {
            return Err(std::io::Error::new(
                std::io::ErrorKind::InvalidInput,
                "ERREUR : La chaîne est invalide.",
            ));
        }

        if c != '0' && c != '1' {
            return Err(std::io::Error::new(
                std::io::ErrorKind::InvalidInput,
                "ERREUR : La chaîne est invalide.",
            ));
        }
    }

    let mut res: i32 = 0;

    for (i, c) in binary_word.chars().enumerate() {
        let bit: i32 = match c {
            c if c == '0' => 0,
            _ => 1,
        };

        let base: i32 = 2;

        res += bit * base.pow((binary_word.len() - i - 1) as u32);
    }

    return Ok(res);
}

pub fn decimal_to_binary(nb: i32) -> String {
    let mut res: String = String::from("");
    let mut q: i32 = nb;
    let mut r: i32;
    let base: i32 = 2;

    while q > 0 {
        r = q % base;
        q = q / base;

        res += &r.to_string();
    }

    let tmp: String = res.chars().rev().collect::<String>();
    res = tmp;

    return res;
}

pub fn hexadecimalVersDecimal(hexa: &String) -> Result<i32, std::io::Error> {
    let hexa_temp = hexa.to_lowercase();

    if hexa_temp.is_empty() {
        return Err(std::io::Error::new(
            std::io::ErrorKind::InvalidInput,
            "ERREUR : La chaîne est vide.",
        ));
    }

    for c in hexa_temp.chars() {
        if !c.is_digit(10) && c != 'a' && c != 'b' && c != 'c' && c != 'd' && c != 'e' && c != 'f' {
            return Err(std::io::Error::new(
                std::io::ErrorKind::InvalidInput,
                "ERREUR : La chaîne est invalide.",
            ));
        }
    }

    let mut res: i32 = 0;

    for (i, c) in hexa_temp.chars().enumerate() {
        let number: i32 = match c {
            c if c.is_digit(10) => c.to_digit(10).unwrap() as i32,
            c if c == 'a' => 10,
            c if c == 'b' => 11,
            c if c == 'c' => 12,
            c if c == 'd' => 13,
            c if c == 'e' => 14,
            _ => 15,
        };

        let base: i32 = 16;

        res += number * base.pow((hexa_temp.len() - i - 1) as u32);
    }

    return Ok(res);
}

pub fn decimal_to_hexa(nb: i32) -> String {
    let mut res: String = String::from("");
    let mut q: i32 = nb;
    let mut r: i32;
    let base: i32 = 16;

    while q > 0 {
        r = q % base;
        q = q / base;

        let temp: String = match r {
            r if r < 10 => r.to_string(),
            r if r == 10 => String::from("a"),
            r if r == 11 => String::from("b"),
            r if r == 12 => String::from("c"),
            r if r == 13 => String::from("d"),
            r if r == 14 => String::from("e"),
            _ => String::from("f"),
        };

        res += &temp;
    }

    let tmp: String = res.chars().rev().collect::<String>();
    res = tmp;

    return res;
}

fn bits_to_target(bits: u32) -> [u8; 32] {
    let mut target: [u8; 32] = [0x00; 32];

    let exp : usize = (bits / 0xffffff).try_into().unwrap();
    let mut byte: u8;
    let mut bytes: u32 = bits % 16777216;

    for i in (0..3).rev() {
        byte = (bytes % 256) as u8;
        bytes = bytes / 256;
        target[32 - exp + i] = byte;
    }

    target
}

pub fn satoshi_to_bitcoin(satoshi: i64) -> f64 {
    let div: i64 = i64::pow(10, 8).into();
    let res: f64 = (satoshi as f64) / (div as f64);
    res
}

fn display_time(time: u32) -> chrono::DateTime<chrono::Utc> {
    let naive = NaiveDateTime::from_timestamp(time as i64, 0);
    let datetime: DateTime<Utc> = DateTime::from_utc(naive, Utc);
    let newdate = datetime.format("%Y-%m-%d %H:%M:%S");
    println!("{}", newdate);
    datetime
}

pub fn date_to_timestamp(dateStr: &String) -> Result<i64, std::io::Error> // ERREURS DE CONVERSION VERS LA BLOCKHAIN (i64 -> u32).
{
    let time = match NaiveDateTime::parse_from_str(dateStr, "%Y-%m-%d") {
        Ok(tmp) => tmp,
        _ => {
            return Err(std::io::Error::new(
                std::io::ErrorKind::InvalidInput,
                "ERREUR : La chaîne est invalide.",
            ));
        }
    };

    let datetime: DateTime<Utc> = DateTime::from_utc(time, Utc);

    return Ok(datetime.timestamp());
}

pub fn timestamp_to_time(time: i64) -> chrono::DateTime<chrono::Utc> {
    let naive = NaiveDateTime::from_timestamp(time as i64, 0);
    let datetime: DateTime<Utc> = DateTime::from_utc(naive, Utc);

    return datetime;
}

pub fn equal_date(
    date1: chrono::DateTime<chrono::Utc>,
    date2: chrono::DateTime<chrono::Utc>,
) -> bool {
    return (date1.year() == date2.year())
        && (date1.month() == date2.month())
        && (date1.day() == date2.day());
}

pub fn lower_date(
    date1: chrono::DateTime<chrono::Utc>,
    date2: chrono::DateTime<chrono::Utc>,
) -> bool {
    return (date1.year() < date2.year())
        && (date1.month() < date2.month())
        && (date1.day() < date2.day());
}

pub fn higher_date(
    date1: chrono::DateTime<chrono::Utc>,
    date2: chrono::DateTime<chrono::Utc>,
) -> bool {
    return (date1.year() > date2.year())
        && (date1.month() > date2.month())
        && (date1.day() > date2.day());
}

pub fn lower_equal_date(
    date1: chrono::DateTime<chrono::Utc>,
    date2: chrono::DateTime<chrono::Utc>,
) -> bool {
    return (date1.year() <= date2.year())
        && (date1.month() <= date2.month())
        && (date1.day() <= date2.day());
}

pub fn higher_equal_date(
    date1: chrono::DateTime<chrono::Utc>,
    date2: chrono::DateTime<chrono::Utc>,
) -> bool {
    return (date1.year() >= date2.year())
        && (date1.month() >= date2.month())
        && (date1.day() >= date2.day());
}
