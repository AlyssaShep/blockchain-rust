#![allow(non_snake_case)]

use crate::structure::block_struct::Block;
use crate::structure::transaction_struct::Transaction;
use crate::traitements::interpretations_functions::calcul_merkle_root_attente;

use sha2::{Digest, Sha256};
use std::fmt::Write;

use std::{
    io,
    sync::{Arc, Mutex},
    thread,
};

use tui::{
    backend::{Backend, CrosstermBackend},
    Terminal,
};

use crossterm::{
    execute,
    terminal::{enable_raw_mode, EnterAlternateScreen, ScrollDown, ScrollUp},
};

pub struct Node {
    hash: String,
    depth: usize,
}

impl Node {
    fn new(hash: String, depth: usize) -> Node {
        Node {
            hash: hash,
            depth: depth,
        }
    }
}

pub fn parse_hex(hex_asm: &str) -> Vec<u8> {
    let hex_chars: Vec<char> = hex_asm
        .as_bytes()
        .iter()
        .filter_map(|b| {
            let ch = char::from(*b);
            if ('0' <= ch && ch <= '9') || ('a' <= ch && ch <= 'f') || ('A' <= ch && ch <= 'F') {
                Some(ch)
            } else {
                None
            }
        })
        .collect();

    let mut index = 0usize;
    let (odd_chars, even_chars): (Vec<char>, Vec<char>) = hex_chars.into_iter().partition(|_| {
        index = index + 1;
        index % 2 == 1
    });

    odd_chars
        .into_iter()
        .zip(even_chars.into_iter())
        .map(|(c0, c1)| {
            fn hexchar2int(ch: char) -> u8 {
                if '0' <= ch && ch <= '9' {
                    ch as u8 - '0' as u8
                } else {
                    0xa + if 'a' <= ch && ch <= 'f' {
                        ch as u8 - 'a' as u8
                    } else if 'A' <= ch && ch <= 'F' {
                        ch as u8 - 'A' as u8
                    } else {
                        unreachable!()
                    }
                }
            }
            hexchar2int(c0) * 0x10 + hexchar2int(c1)
        })
        .collect::<Vec<u8>>()
}

pub fn create_leaves(transactions: &Vec<Transaction>) -> Vec<String> {
    let mut leaves: Vec<String> = Vec::new();
    for transaction in transactions.iter() {
        leaves.push(transaction.tx_id_to_string());
    }
    leaves
}

pub fn create_node(sha1: String, sha2: String) -> String {
    let sha1: Vec<u8> = hex::decode(sha1).expect("Invalid Hex String");
    let sha2: Vec<u8> = hex::decode(sha2).expect("Invalid Hex String");
    let mut hasher = Sha256::new();
    hasher.update(sha1);
    hasher.update(sha2);
    let node: String = format!("{:x}", hasher.finalize());
    let node: Vec<u8> = hex::decode(node).expect("Invalid Hex String");
    hasher = Sha256::new();
    hasher.update(node);
    let node: String = format!("{:x}", hasher.finalize());
    node
}

pub fn create_root(leaves: &mut Vec<String>) -> Vec<Node> {
    let mut tree_width: Vec<Node> = Vec::new(); // contient tous les noeuds comme dans un parcours en largeur d'un arbre
    let mut depth: usize = 0;
    if leaves.len() == 1 {
        tree_width.push(Node::new(leaves[0].clone(), depth));
        return tree_width;
    }
    while leaves.len() > 1 {
        if leaves.len() % 2 == 1 {
            // si la taille du tableau est impaire, il faudra hasher la dernière transaction, avec elle-même
            leaves.push(leaves[leaves.len() - 1].clone());
        }
        let mut i = 0;
        while i < leaves.len() {
            tree_width.push(Node::new(leaves[i].clone(), depth));
            tree_width.push(Node::new(leaves[i + 1].clone(), depth));
            leaves[i] = create_node(leaves[i].clone(), leaves[i + 1].clone());
            i += 2;
        }
        i = leaves.len() - 1;
        while i > 0 {
            leaves.remove(i);
            i -= 1;
            if i != 0 {
                i -= 1;
            }
        }
        depth += 1;
    }
    tree_width.push(Node::new(leaves[0].clone(), depth));
    tree_width
}

pub fn creer_arbre(block: &Block) -> Vec<Node> {
    let mut leaves: Vec<String> = create_leaves(block.transaction()); // on prend chaque transaction qu'on consière comme une feuille
    let tree: Vec<Node> = create_root(&mut leaves);
    tree
}

pub fn verifier_merkle_root(block: &Block) -> bool {
    let merkle_root_retrieved = String::from(block.get_block_header().merkle_root_to_string()); // la merkle root telle qu'elle a été extraite
                                                                                               // println!("merkle_root_extraite {}", &merkle_root_extraite);
    let temp = parse_hex(&merkle_root_retrieved);
    let mut merkle_root_retrieved_reverse = String::new(); // on l'inverse
    for i in temp.iter().rev() {
        write!(merkle_root_retrieved_reverse, "{:02x}", i).unwrap();
    }
    let tree: Vec<Node> = creer_arbre(&block);
    merkle_root_retrieved_reverse.eq(&tree[tree.len() - 1].hash)
}

pub fn verifier_merkle_root_eclatee<B: Backend>(
    id_start: usize,
    id_end: usize,
    blocks: Arc<Vec<Block>>,
    res_verifs: Arc<Mutex<Vec<bool>>>,
    tab_verifs: Arc<Mutex<Vec<(usize, bool)>>>,
    mutex_progress_clone: Arc<Mutex<usize>>,
    mutex_val_loop: Arc<Mutex<u16>>,
    mutex_terminal: Arc<Mutex<Terminal<B>>>,
    file_name: Arc<String>,
 ) -> Result<(), std::io::Error> {
    let mut id_start_local: usize = id_start;
    let id_end_local: usize = id_end;

    let mut verified: bool = true;

    let nb_blk_total = blocks.len();

    while id_start_local < id_end_local {
        let merkle_root_retrieved = String::from(
            blocks[id_start_local]
                .get_block_header()
                .merkle_root_to_string(),
        ); // la merkle root telle qu'elle a été extraite

        let temp = parse_hex(&merkle_root_retrieved);
        let mut merkle_root_retrieved_reverse = String::new(); // on l'inverse

        for i in temp.iter().rev() {
            write!(merkle_root_retrieved_reverse, "{:02x}", i).unwrap();
        }

        let tree: Vec<Node> = creer_arbre(&blocks[id_start_local]);

        let local_verif: bool =
        merkle_root_retrieved_reverse.eq(&tree[tree.len() - 1].hash);

            verified = (verified) && (local_verif);

        let _ = &tab_verifs.lock().unwrap().push((id_start_local, local_verif));

        id_start_local += 1;

        let mut nb_blk = mutex_progress_clone.lock().unwrap();
        *nb_blk += 1;

        let mut val_l = mutex_val_loop.lock().unwrap();
        *val_l = (*nb_blk * 100 / nb_blk_total) as u16;
    }

    let mut terminal = mutex_terminal.lock().unwrap();
    let mut val_l = mutex_val_loop.lock().unwrap();

    terminal.draw(|f| calcul_merkle_root_attente(f, &mut val_l, file_name.to_string()))?;

    let _ = &res_verifs.lock().unwrap().push(verified);

    return Ok(());
}

pub fn verifier_merkle_root_threads(
    blocks: &Vec<Block>,
    tab_res_verifications : &mut Vec<(usize, bool)>,
    file_name: String,
 ) -> Result<bool, std::io::Error> {
    enable_raw_mode()?;
    let mut stdout = io::stdout();

    execute!(
        stdout,
        EnterAlternateScreen,
        ScrollUp(1000),
        ScrollDown(1000)
    )?;

    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    let mut val_loop: u16 = 0;
    let progress: usize = 0;

    terminal.draw(|f| calcul_merkle_root_attente(f, &mut val_loop, file_name.clone()))?;

    let number_threads: usize = 15; // Nombre de threads, pour le moment créé comme constante locale à cette fonction.

    if blocks.len() < number_threads
    // Si jamais le nombre de blocks dans un fichier est inférieur au nombre de threads,
    {
        // alors il n'y a pas besoin de créer de threads et le fichier est lu dans le thread principal.
        let mut res_verifs: bool = true;

        for i in 0..blocks.len() {
            res_verifs = (res_verifs) && (verifier_merkle_root(&blocks[i]));
        }

        return Ok(res_verifs);
    }

    let mut tab_threads_handlers = vec![]; // Le tableau de threads.

    let number_blocks_per_threads = blocks.len() / number_threads; // Le nombre de blocks traités par threads, excepté pour le dernier thread
                                                                  // qui devra traité le nombre de blocks restant, qui peut être différent.

    let mut tab_blocks_temp: Vec<Block> = Vec::with_capacity(blocks.len());

    for i in 0..blocks.len() {
        tab_blocks_temp.push(blocks[i].clone());
    }

    let blocks_to_pass = Arc::new(tab_blocks_temp);

    let file_name_th = Arc::new(file_name);

    let tab_res_verifs: Vec<bool> = Vec::with_capacity(number_threads);

    let mutex_tab_res_verifs = Arc::new(Mutex::new(tab_res_verifs));

    let mutex_progress = Arc::new(Mutex::new(progress));

    let mutex_terminal = Arc::new(Mutex::new(terminal));

    let mutex_val_loop = Arc::new(Mutex::new(val_loop));

    let tab_verifs: Vec<(usize, bool)> = Vec::with_capacity(blocks.len());

    let mutex_verifs = Arc::new(Mutex::new(tab_verifs));

    let mut local_id_start: usize = 0;
    let mut local_id_end: usize = 0;

    for i in 0..number_threads {
        if i < (number_threads - 1) {
            for _ in (i * number_blocks_per_threads)
                ..(number_blocks_per_threads + (i * number_blocks_per_threads))
            {
                local_id_end += 1;
            }
        } else {
            for _ in (i * number_blocks_per_threads)..blocks.len() {
                local_id_end += 1;
            }
        }

        let blocks_to_pass_clone = Arc::clone(&blocks_to_pass);
        let fichier_clone = Arc::clone(&file_name_th);
        let mutex_tab_res_verifs_clone = Arc::clone(&mutex_tab_res_verifs);
        let mutex_verifs_clone = Arc::clone(&mutex_verifs);
        let mutex_avancement_clone = Arc::clone(&mutex_progress);
        let mutex_val_loop_clone = Arc::clone(&mutex_val_loop);
        let mutex_terminal_clone = Arc::clone(&mutex_terminal);

        tab_threads_handlers.push(thread::spawn(move || {
            verifier_merkle_root_eclatee(
                local_id_start,
                local_id_end,
                blocks_to_pass_clone,
                mutex_tab_res_verifs_clone,
                mutex_verifs_clone,
                mutex_avancement_clone,
                mutex_val_loop_clone,
                mutex_terminal_clone,
                fichier_clone,
            )
            .unwrap();
        }));

        local_id_start = local_id_end;
    }

    for processus in tab_threads_handlers {
        let _truc = match processus.join() {
            Ok(truc) => truc,
            Err(_e) => return Err(
                        std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "ERREUR : Une erreur s'est produite lors des vérification au niveau de l'arbre de Merkle.",
                        )
                    ),
        };
    }

    let tab_res_verifs = mutex_tab_res_verifs.lock().unwrap();
    let mut res_verifs: bool = true;

    for i in 0..tab_res_verifs.len() {
        res_verifs = (res_verifs) && (tab_res_verifs[i]);
    }

    let tab_verifs = mutex_verifs.lock().unwrap();

    for i in 0..tab_verifs.len() {
        tab_res_verifications.push(tab_verifs[i]);
    }

    return Ok(res_verifs);
}

pub fn display_tree(block: &Block) {
    let tree: Vec<Node> = creer_arbre(&block);
    let height = tree[tree.len() - 1].depth;
    for node in tree.iter().rev() {
        if node.depth == height {
            println!("·{}", node.hash);
        } else if node.depth == height - 1 {
            println!(" ❘――{}", node.hash);
        } else {
            print!("{: <1$}", "", (height - node.depth) * 3 - 2);
            print!("❘――");
            println!("{}", node.hash);
        }
    }
}
