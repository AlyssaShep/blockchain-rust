use crate::structure::block_struct::Block as Blk;
use crate::traitements::merkle_tree::verifier_merkle_root_threads;
use crate::traitements::recherches::search_for_highest_value_per_block;
use crate::traitements::recherches::HighestTransaction;

use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode},
    execute,
    terminal::{
        disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen, ScrollDown,
        ScrollUp,
    },
};

use std::{
    error::Error,
    io,
    time::{Duration, Instant},
};

use tui::{
    backend::{Backend, CrosstermBackend},
    layout::{Alignment, Constraint, Direction, Layout, Rect},
    style::{Color, Modifier, Style},
    symbols,
    text::{Span, Spans},
    widgets::{
        Axis, Block, Borders, Chart, Dataset, Gauge, GraphType, List, ListItem, ListState,
        Paragraph, Tabs, Wrap,
    },
    Frame, Terminal,
};

struct StatefulList<T> {
    state: ListState,
    items: Vec<T>,
}

impl<T> StatefulList<T> {
    fn with_items(items: Vec<T>) -> StatefulList<T> {
        StatefulList {
            state: ListState::default(),
            items,
        }
    }

    fn previous(&mut self) {
        let i = match self.state.selected() {
            Some(i) => {
                if i >= self.items.len() - 1 {
                    0
                } else {
                    i + 1
                }
            }
            None => 0,
        };
        self.state.select(Some(i));
    }

    fn next(&mut self) {
        let i = match self.state.selected() {
            Some(i) => {
                if i == 0 {
                    self.items.len() - 1
                } else {
                    i - 1
                }
            }
            None => 0,
        };
        self.state.select(Some(i));
    }

    fn unselect(&mut self) {
        self.state.select(None);
    }
}

struct AppPara {
    scroll: u16,
}

impl AppPara {
    fn new() -> AppPara {
        AppPara { scroll: 0 }
    }

    fn on_tick(&mut self) {
        self.scroll += 1;
        self.scroll %= 10;
    }
}

struct Stat {
    average_value: f64,
    mediane: f64,
    quartile: f64,
    quartile3: f64,
    volume: f64,
}

impl Stat {
    fn new_defaut() -> Stat {
        return Stat {
            average_value: 0.0,
            mediane: 0.0,
            quartile: 0.0,
            quartile3: 0.0,
            volume: 0.0,
        };
    }
    fn new(blks: &Vec<Blk>) -> Vec<Stat> {
        let mut stats: Vec<Stat> = Vec::new();

        for i in blks {
            let moy: f64 = i.average_value();
            let mut med: f64 = 0.0;
            let mut quartile_1: f64 = 0.0;
            let mut quartile_3: f64 = 0.0;
            let mut vol: f64 = 0.0;

            i.stat(&mut quartile_1, &mut med, &mut quartile_3, &mut vol);

            let stat = Stat {
                average_value: moy,
                mediane: med,
                quartile: quartile_1,
                quartile3: quartile_3,
                volume: vol,
            };

            stats.push(stat);
        }

        return stats;
    }

    fn volume(&self) -> &f64 {
        return &self.volume;
    }

    fn quartile_3(&self) -> &f64 {
        return &self.quartile3;
    }

    fn quartile(&self) -> &f64 {
        return &self.quartile;
    }

    fn mediane(&self) -> &f64 {
        return &self.mediane;
    }

    fn average_value(&self) -> &f64 {
        return &self.average_value;
    }
}

struct App<'a> {
    data: Vec<(f64, f64)>,
    max_nb_transactions: f64,

    vec_highest_transactions: Vec<(f64, f64)>,
    highest_value_file: f64,

    index: usize,

    block_index: usize,

    stats: Vec<Stat>,

    events: StatefulList<(&'a str, &'a str)>,
    verification: Vec<(usize, bool)>,
    verification_file: StatefulList<(String, String)>,
}

impl<'a> App<'a> {
    fn new_defaut() -> App<'a> {
        App {
            data: Vec::new(),
            max_nb_transactions: 0.0,
            vec_highest_transactions: Vec::new(),
            highest_value_file: 0.0,
            index: 0,
            block_index: 0,
            stats: Vec::new(),
            events: StatefulList::with_items(vec![("Item0", "truc")]),
            verification: Vec::new(),
            verification_file: StatefulList::with_items(vec![(
                "Item0".to_string(),
                "truc".to_string(),
            )]),
        }
    }
    fn new<B: Backend>(
        blk: &Vec<Blk>,
        files: &'a Vec<String>,
        files_verif: &'a Vec<String>,
        it_index: usize,
        terminal: &mut Terminal<B>,
        verif: &mut Vec<(String, bool)>,
    ) -> Result<App<'a>, Box<dyn Error>> {

        let mut data: Vec<(f64, f64)> = Vec::new();

        let mut fichier_and_status: Vec<(&'a str, &'a str)> = Vec::new();
        let mut highest_value: f64 = 0.0;

        let mut val_loop: u16 = 0;

        let mut verif_f: Vec<(usize, bool)> = Vec::new();

        let mut it_id: usize = 0;
        let mut m_nb_transactions: usize = 0;
        let mut min_time: u32 = *blk[0].get_block_header().get_time();
        let mut max_time: u32 = *blk[0].get_block_header().get_time();

        terminal.draw(|f| calcul_merkle_root_attente(f, &mut val_loop, files[it_index].clone()))?;

        for it in blk {
            data.push((it_id as f64, *it.tx_count() as f64));

            if *it.tx_count() as usize > m_nb_transactions {
                m_nb_transactions = *it.tx_count() as usize;
            }

            if *it.get_block_header().get_time() > max_time {
                max_time = *it.get_block_header().get_time();
            }

            if *it.get_block_header().get_time() < min_time {
                min_time = *it.get_block_header().get_time();
            }

            it_id += 1;
        }

        let verif_file_threads =
            verifier_merkle_root_threads(blk, &mut verif_f, files[it_index].clone()).unwrap();

        it_id = 0;

        if files_verif.is_empty() {
            for it in files {
                if it_id == it_index {
                    fichier_and_status.push((it, "En cours"));
                    verif.push((it.clone(), verif_file_threads));
                } else {
                    fichier_and_status.push((it, "En attente"));
                }

                it_id += 1;
            }
        } else {
            for it in files {
                let verif_file = files_verif.contains(&it);

                if it_id == it_index && verif_file {
                    fichier_and_status.push((it, "En cours - Selectionné"));
                    verif.push((it.clone(), verif_file_threads));
                } else if it_id < it_index || verif_file {
                    fichier_and_status.push((it, "Traité"));
                } else {
                    fichier_and_status.push((it, "En attente"));
                }

                it_id += 1;
            }
        }

        let mut appends: Vec<(String, String)> = Vec::new();

        for i in &fichier_and_status {
            if verif.iter().any(|(a, _b)| a == i.0) {
                let idv = verif.contains(&((i.0).to_string(), true));
                if idv == true {
                    appends.push((i.0.to_string(), String::from("Verifié")));
                } else {
                    appends.push((i.0.to_string(), String::from("Erroné")));
                }
            } else {
                appends.push((i.0.to_string(), String::from("Non vérifié")));
            }
        }

        let appends_list_items = StatefulList::with_items(appends);

        let data2: Vec<HighestTransaction> =
            search_for_highest_value_per_block(&blk, &mut highest_value);

        let mut vec_highest_transactions: Vec<(f64, f64)> = Vec::new();
        let mut id: f64 = 0.0;
        for it in data2 {
            vec_highest_transactions.push((id, it.get_value() as f64));
            id += 1.0;
        }

        Ok(App {
            data,
            max_nb_transactions: m_nb_transactions as f64,
            vec_highest_transactions,
            highest_value_file: highest_value,
            index: 0,
            block_index: 0,
            stats: Stat::new(&blk),
            events: StatefulList::with_items(fichier_and_status),
            verification: verif_f,
            verification_file: appends_list_items,
        })
    }

    fn on_tick(&mut self) {
        let value = self.data.pop().unwrap();
        self.data.insert(0, value);

        let value2 = self.vec_highest_transactions.pop().unwrap();
        self.vec_highest_transactions.insert(0, value2);
    }

    pub fn block_select(&self) -> &usize {
        return &self.block_index;
    }

    pub fn block_select_suivant(&mut self) {
        self.block_index = (self.block_index + 1) % self.data.len();
    }

    pub fn block_select_precedent(&mut self) {
        if self.block_index > 0 {
            self.block_index -= 1;
        } else {
            self.block_index = self.data.len() - 1;
        }
    }

    pub fn help(&mut self) {
        self.index = 4;
    }

    pub fn next(&mut self) {
        self.index = (self.index + 1) % 5;
    }

    pub fn previous(&mut self) {
        if self.index > 0 {
            self.index -= 1;
        } else {
            self.index = 5 - 1;
        }
    }
}

pub fn function_test(
    blk: &Vec<Blk>,
    files: &Vec<String>,
    files_verif: &Vec<String>,
    it_index: &mut usize,
    out: &mut bool,
    write: &mut bool,
    verif: &mut Vec<(String, bool)>,
 ) -> Result<(), Box<dyn Error>> {
    
    enable_raw_mode()?;
    let mut stdout = io::stdout();

    execute!(
        stdout,
        EnterAlternateScreen,
        EnableMouseCapture,
        ScrollUp(1000),
        ScrollDown(1000)
    )?;

    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    // create app and run it
    let tick_rate = Duration::from_millis(250);

    let app: App = App::new(&blk, &files, &files_verif, *it_index, &mut terminal, verif)?;
    let app_p: AppPara = AppPara::new();

    let res = run(
        &mut terminal,
        app,
        app_p,
        tick_rate,
        out,
        write,
        it_index,
        blk,
    );

    // restore terminal
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    terminal.show_cursor()?;

    if let Err(err) = res {
        println!("{:?}", err)
    }

    Ok(())
}

fn run<B: Backend>(
    terminal: &mut Terminal<B>,
    mut app: App,
    mut app_p: AppPara,
    tick_rate: Duration,
    out: &mut bool,
    write: &mut bool,
    it_index: &mut usize,
    blk: &Vec<Blk>,
 ) -> io::Result<()> {
    let mut last_tick = Instant::now();

    terminal.clear()?;

    loop {
        terminal.draw(|f| draw(f, &mut app, &blk))?;

        let timeout = tick_rate
            .checked_sub(last_tick.elapsed())
            .unwrap_or_else(|| Duration::from_secs(0));

        if crossterm::event::poll(timeout)? {
            if let Event::Key(key) = event::read()? {
                match key.code {
                    KeyCode::Char('q') => {
                        *out = true;
                        return Ok(());
                    }
                    KeyCode::Char('Q') => {
                        *out = true;
                        return Ok(());
                    }
                    KeyCode::Char('e') => {
                        *it_index += 1;
                        *write = true;
                        return Ok(());
                    }
                    KeyCode::Char('E') => {
                        *write = true;
                        *out = true;
                        return Ok(());
                    }
                    KeyCode::Right => {
                        app.events.unselect();
                        app.verification_file.unselect();
                        app.next();
                    }
                    KeyCode::Left => {
                        app.events.unselect();
                        app.verification_file.unselect();
                        app.previous();
                    }
                    KeyCode::Up => {
                        app.events.next();
                        app.verification_file.next();
                    }
                    KeyCode::Down => {
                        app.events.previous();
                        app.verification_file.previous();
                    }
                    KeyCode::Char('s') => {
                        *it_index += 1;
                        return Ok(());
                    }
                    KeyCode::Char('S') => {
                        *it_index += 1;
                        return Ok(());
                    }
                    KeyCode::Char('p') => {
                        if *it_index == 0 {
                            *out = true;
                        } else {
                            *it_index -= 1;
                        }
                        return Ok(());
                    }
                    KeyCode::Char('P') => {
                        if *it_index == 0 {
                            *out = true;
                            *write = true;
                        } else {
                            *it_index -= 1;
                        }
                        return Ok(());
                    }
                    KeyCode::Char('h') => app.help(),
                    KeyCode::Char('H') => app.help(),
                    KeyCode::Char('+') => app.block_select_suivant(),
                    KeyCode::Char('-') => app.block_select_precedent(),
                    _ => {}
                }
            }
        }

        if last_tick.elapsed() >= tick_rate {
            app.on_tick();
            app_p.on_tick();
            last_tick = Instant::now();
        }
    }
}

fn draw<B: Backend>(f: &mut Frame<B>, app: &mut App, blk: &Vec<Blk>) {
    let mut titles: Vec<Span> = Vec::default();

    titles.push(Span::raw(String::from("Présentation")));
    titles.push(Span::raw(String::from("Blocs")));
    titles.push(Span::raw(String::from("Transactions")));
    titles.push(Span::raw(String::from("BTC")));
    titles.push(Span::raw(String::from("Aide")));

    let mut spans: Vec<Spans> = Vec::default();
    spans.push(Spans::from(Span::clone(&titles[0])));
    spans.push(Spans::from(Span::clone(&titles[1])));
    spans.push(Spans::from(Span::clone(&titles[2])));
    spans.push(Spans::from(Span::clone(&titles[3])));
    spans.push(Spans::from(Span::clone(&titles[4])));

    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .margin(0)
        .constraints(
            [
                Constraint::Percentage(15),
                Constraint::Ratio(1, 2),
                Constraint::Ratio(1, 3),
                Constraint::Percentage(15),
                Constraint::Length(5),
                Constraint::Max(5),
                Constraint::Min(0),
            ]
            .as_ref(),
        )
        .split(f.size());

    let rect = Rect::new(chunks[0].x, chunks[0].y, chunks[0].width, chunks[0].height);

    let tabs = Tabs::new(spans)
        .block(Block::default().borders(Borders::ALL).title("Onglets"))
        .highlight_style(Style::default().fg(Color::Yellow))
        .select(app.index);
    f.render_widget(tabs, rect);

    match app.index {
        0 => ui_tabs_0(f, app, &chunks),
        1 => ui_tabs_1(f, app, &blk),
        2 => ui_tabs_2(f, &app),
        3 => ui_tabs_3(f, &app),
        4 => help_tabs_4(f, &chunks),
        _ => {}
    };
}

pub fn run_write(
    it_index: &usize,
    json: &mut bool,
    txt: &mut bool,
    out_files: &mut Vec<String>,
 ) -> io::Result<()> {
    enable_raw_mode()?;
    let mut stdout = io::stdout();

    execute!(
        stdout,
        EnterAlternateScreen,
        EnableMouseCapture,
        ScrollUp(1000),
        ScrollDown(1000)
    )?;

    let last_tick = Instant::now();
    let tick_rate = Duration::from_millis(250);

    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    let timeout = tick_rate
        .checked_sub(last_tick.elapsed())
        .unwrap_or_else(|| Duration::from_secs(0));

    terminal.clear()?;

    loop {
        terminal.draw(|f| draw_ecriture(f))?;

        if crossterm::event::poll(timeout)? {
            if let Event::Key(key) = event::read()? {
                match key.code {
                    KeyCode::Char('q') => {
                        terminal.clear()?;
                        return Ok(());
                    }
                    KeyCode::Char('Q') => {
                        terminal.clear()?;
                        return Ok(());
                    }

                    KeyCode::Char('t') => {
                        out_files[it_index - 1].push_str(".txt");
                        *txt = true;
                        return Ok(());
                    }
                    KeyCode::Char('T') => {
                        out_files[it_index - 1].push_str(".txt");
                        *txt = true;
                        return Ok(());
                    }

                    KeyCode::Char('j') => {
                        out_files[it_index - 1].push_str(".json");
                        *json = true;
                        return Ok(());
                    }
                    KeyCode::Char('J') => {
                        out_files[it_index - 1].push_str(".json");
                        *json = true;
                        return Ok(());
                    }
                    _ => {}
                }
            }
        }
    }
}

fn draw_ecriture<B: Backend>(f: &mut Frame<B>) {
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .margin(0)
        .constraints(
            [
                Constraint::Percentage(100),
                Constraint::Ratio(1, 1),
                Constraint::Max(5),
                Constraint::Min(0),
            ]
            .as_ref(),
        )
        .split(f.size());

    let mut info = String::from("Option d'écriture : ");
    info.push_str("\n");
    info.push_str("- Exporter au format .txt : t/T");
    info.push_str("\n");
    info.push_str("- Exporter au format .json : j/J");
    info.push_str("\n");
    info.push_str("- Sortie : q/Q");
    info.push_str("\n");

    let paragraph = Paragraph::new(info)
        .block(Block::default().borders(Borders::ALL).title("Ecriture"))
        .alignment(Alignment::Center)
        .wrap(Wrap { trim: true });

    let rect = Rect::new(chunks[0].x, chunks[0].y, chunks[0].width, chunks[0].height);
    f.render_widget(paragraph, rect);
}

fn ui_tabs_0<B: Backend>(f: &mut Frame<B>, app: &mut App, chunks: &Vec<Rect>) {
    let chunks_2 = Layout::default()
        .direction(Direction::Vertical)
        .vertical_margin(0)
        .constraints(
            [
                Constraint::Percentage(70),
                Constraint::Ratio(1, 6),
                Constraint::Max(5),
                Constraint::Min(0),
            ]
            .as_ref(),
        )
        .split(f.size());

    let gauge = Gauge::default()
        .block(
            Block::default()
                .title("Barre de progression")
                .borders(Borders::ALL),
        )
        .gauge_style(Style::default().fg(Color::Green))
        .percent(100);

    let rect_1 = Rect::new(
        chunks_2[1].x,
        chunks_2[1].y,
        chunks_2[1].width,
        chunks_2[1].height,
    );
    f.render_widget(gauge, rect_1);

    let info1 = String::from("Vous trouverez dans les onglets des graphiques d'interprétations ou des informations générales sur un bloc sélectionné.");
    let info2 = String::from("Permet la lecture de fichiers .dat issu de la Blockchain Bitcoin");
    let info3 = String::from("Utilisation: cargo run --release [choix] [dossier] [dossier]");
    let info4 = String::from("  d, defaut, default");
    let info5 = String::from("  o, out [chemin_sortie]");
    let info6 = String::from("  i, in [chemin_entrée]");
    let info7 = String::from("  io, inout [chemin_entrée] [chemin_sortie]");
    let info8 = String::from("Pour plus d'informations : cargo run aide");
    let info9 =
        String::from("Si vous avez besoin d'aides sur la navigation, allez dans l'onglet Aide.");
    let info10 = String::from("Pour afficher l'aide : h/H");
    let space = String::from("");

    let text = vec![
        Spans::from(info1),
        Spans::from(space.clone()),
        Spans::from(info2),
        Spans::from(space.clone()),
        Spans::from(info3),
        Spans::from(info4),
        Spans::from(info5),
        Spans::from(info6),
        Spans::from(info7),
        Spans::from(info8),
        Spans::from(space.clone()),
        Spans::from(info9),
        Spans::from(info10),
    ];
    let paragraph = Paragraph::new(text)
        .block(Block::default().borders(Borders::ALL).title("Intro"))
        .alignment(Alignment::Left)
        .wrap(Wrap { trim: true });

    let rect_2 = Rect::new(
        chunks[1].x,
        chunks[1].y,
        chunks[1].width / 2,
        chunks[1].height,
    );
    f.render_widget(paragraph, rect_2);

    let mut index: usize = 0;

    let items: Vec<ListItem> = app
        .events
        .items
        .iter()
        .map(|i| {
            let s = match i.1 {
                "En cours" => Style::default().fg(Color::Yellow),
                "En cours - Selectionné" => Style::default().fg(Color::Yellow),
                "En attente" => Style::default().fg(Color::Red),
                "Traité" => Style::default().fg(Color::Green),
                _ => Style::default(),
            };

            let mut lines = vec![];

            index += 1;

            lines.push(Spans::from(Span::styled(
                index.to_string(),
                Style::default().add_modifier(Modifier::ITALIC),
            )));

            lines.push(Spans::from(i.0.clone()));

            lines.push(Spans::from(Span::styled(format!("{:<9}", i.1), s)));

            lines.push(Spans::from(Span::raw(" ")));

            ListItem::new(lines).style(Style::default().fg(Color::Black))
        })
        .collect();

    let size = &app.events.items.len().to_string();

    let mut title = String::from("Status des traitements /");
    title.push_str(" ");
    title.push_str(size);

    let items = List::new(items)
        .block(Block::default().borders(Borders::ALL).title(title))
        .highlight_style(
            Style::default()
                .bg(Color::Rgb(180, 180, 180))
                .add_modifier(Modifier::BOLD),
        )
        .highlight_symbol(">> ");

    let rect_3 = Rect::new(
        chunks[1].x + chunks[1].width / 2,
        chunks[1].y,
        chunks[1].width / 2,
        chunks[1].height,
    );
    f.render_stateful_widget(items, rect_3, &mut app.events.state);
}

fn ui_tabs_1<B: Backend>(f: &mut Frame<B>, app: &mut App, blk: &Vec<Blk>) {
    let size_vec: usize = app.data.len();
    let str_size_vec = size_vec.to_string();

    let chunks_2 = Layout::default()
        .direction(Direction::Vertical)
        .margin(0)
        .constraints(
            [
                Constraint::Percentage(15),
                Constraint::Ratio(5, 5),
                Constraint::Max(5),
                Constraint::Min(0),
            ]
            .as_ref(),
        )
        .split(f.size());

    let verification: bool = app.verification[*app.block_select()].1;
    let bl = &blk[*app.block_select()];

    let mut info = String::from("Nombre de blocs dans le fichier : ");
    info.push_str(str_size_vec.as_str());
    info.push_str("\n");
    info.push_str("Bloc selectionné : ");
    info.push_str(app.block_select().to_string().as_str());
    info.push_str("\n");
    info.push_str("\n");
    info.push_str("INFORMATIONS SUR LE BLOC");
    info.push_str("\n");
    info.push_str("\n");
    info.push_str("Magic bytes : ");
    info.push_str(bl.get_magic_bytes().to_string().as_str());
    info.push_str("\n");
    info.push_str("Taille du bloc : ");
    info.push_str(&bl.size().to_string());
    info.push_str("\n");
    info.push_str("Nombre de transactions : ");
    info.push_str(bl.tx_count().to_string().as_str());
    info.push_str("\n");
    info.push_str("\n");
    info.push_str("EN-TETE");
    info.push_str("\n");
    info.push_str("\n");
    info.push_str("version : ");
    info.push_str(bl.get_block_header().get_version().to_string().as_str());
    info.push_str("\n");
    info.push_str("Racine de Merkle : ");
    info.push_str(bl.get_block_header().merkle_root_to_string().as_str());
    info.push_str("\n");
    info.push_str("Heure : ");
    info.push_str(
        bl.get_block_header()
            .clone()
            .afficher_horaire()
            .to_rfc2822()
            .as_str(),
    );
    info.push_str("\n");
    info.push_str("bits : ");
    info.push_str(bl.get_block_header().get_bits().to_string().as_str());
    info.push_str("\n");
    info.push_str("nonce : ");
    info.push_str(bl.get_block_header().get_nonce().to_string().as_str());
    info.push_str("\n");
    info.push_str("\n");
    info.push_str("STATISTIQUES");
    info.push_str("\n");
    info.push_str("\n");
    info.push_str("Volume d'échanges : ");
    info.push_str(app.stats[*app.block_select()].volume().to_string().as_str());
    info.push_str("\n");
    info.push_str("Racine de Merkle (verifié ou éronnée) : ");
    if verification {
        info.push_str("verifié");
    } else {
        info.push_str("éronnée");
    }
    info.push_str("\n");
    info.push_str("valeur moyenne : ");
    info.push_str(app.stats[*app.block_select()].average_value().to_string().as_str());
    info.push_str("\n");
    info.push_str("valeur mediane : ");
    info.push_str(app.stats[*app.block_select()].mediane().to_string().as_str());
    info.push_str("\n");
    info.push_str("premier quartile : ");
    info.push_str(
        app.stats[*app.block_select()]
            .quartile()
            .to_string()
            .as_str(),
    );
    info.push_str("\n");
    info.push_str("troisieme quartile : ");
    info.push_str(
        app.stats[*app.block_select()]
            .quartile_3()
            .to_string()
            .as_str(),
    );
    info.push_str("\n");

    let paragraph = Paragraph::new(info)
        .block(
            Block::default()
                .borders(Borders::ALL)
                .title("Informations generales"),
        )
        .alignment(Alignment::Center)
        .wrap(Wrap { trim: true });

    let rect_2 = Rect::new(
        chunks_2[1].x,
        chunks_2[1].y,
        chunks_2[1].width / 2,
        chunks_2[1].height,
    );
    f.render_widget(paragraph, rect_2);

    let mut index: usize = 0;

    let items: Vec<ListItem> = app
        .verification_file
        .items
        .iter()
        .map(|i| {
            let s = match i.1.as_str() {
                "Verifié" => Style::default().fg(Color::Green),
                "Erroné" => Style::default().fg(Color::Red),
                _ => Style::default().fg(Color::Yellow),
            };

            let mut lines = vec![];

            index += 1;

            lines.push(Spans::from(Span::styled(
                index.to_string(),
                Style::default().add_modifier(Modifier::ITALIC),
            )));

            lines.push(Spans::from(i.0.clone()));

            lines.push(Spans::from(Span::styled(format!("{:<9}", i.1), s)));

            lines.push(Spans::from(Span::raw(" ")));

            ListItem::new(lines).style(Style::default().fg(Color::Black))
        })
        .collect();

    let size = &app.events.items.len().to_string();

    let mut title = String::from("Verification RM sur fichier /");
    title.push_str(" ");
    title.push_str(size);

    let items = List::new(items)
        .block(Block::default().borders(Borders::ALL).title(title))
        .highlight_style(
            Style::default()
                .bg(Color::Rgb(180, 180, 180))
                .add_modifier(Modifier::BOLD),
        )
        .highlight_symbol(">> ");

    let boite3 = Rect::new(
        chunks_2[1].x + chunks_2[1].width / 2,
        chunks_2[1].y,
        chunks_2[1].width / 2,
        chunks_2[1].height,
    );
    f.render_stateful_widget(items, boite3, &mut app.events.state);
}

fn ui_tabs_2<B: Backend>(f: &mut Frame<B>, app: &App) {
    let data_slice = &app.data[..];

    let chunks_2 = Layout::default()
        .direction(Direction::Vertical)
        .margin(0)
        .constraints(
            [
                Constraint::Percentage(15),
                Constraint::Ratio(5, 5),
                Constraint::Max(5),
                Constraint::Min(0),
            ]
            .as_ref(),
        )
        .split(f.size());

    let data_sets = vec![Dataset::default()
        .name("nb transactions")
        .marker(symbols::Marker::Dot)
        .graph_type(GraphType::Scatter)
        .style(Style::default().fg(Color::Cyan))
        .data(data_slice)];

    let half = (app.data.len() / 2).to_string();
    let end = app.data.len().to_string();

    let quarter_y = (app.max_nb_transactions / 4.0).trunc().to_string();
    let end_y = app.max_nb_transactions.round().to_string();
    let three_quarter = (3.0 * app.max_nb_transactions / 4.0).trunc().to_string();
    let half_y = (app.max_nb_transactions / 2.0).trunc().to_string();

    let chart = Chart::new(data_sets)
        .block(
            Block::default()
                .borders(Borders::ALL)
                .title("Nombre de transactions par bloc"),
        )
        .x_axis(
            Axis::default()
                .title(Span::styled("Blocs", Style::default().fg(Color::Red)))
                .style(Style::default().fg(Color::Black))
                .bounds([0.0, app.data.len() as f64])
                .labels(
                    ["0", half.as_str(), end.as_str()]
                        .iter()
                        .cloned()
                        .map(Span::from)
                        .collect(),
                ),
        )
        .y_axis(
            Axis::default()
                .title(Span::styled("Nombre", Style::default().fg(Color::Red)))
                .style(Style::default().fg(Color::Black))
                .bounds([0.0, app.max_nb_transactions])
                .labels(
                    [
                        "0",
                        quarter_y.as_str(),
                        half_y.as_str(),
                        three_quarter.as_str(),
                        end_y.as_str(),
                    ]
                    .iter()
                    .cloned()
                    .map(Span::from)
                    .collect(),
                ),
        );

    let rect_1 = Rect::new(
        chunks_2[1].x,
        chunks_2[1].y,
        chunks_2[1].width,
        chunks_2[1].height,
    );
    f.render_widget(chart, rect_1);
}

fn ui_tabs_3<B: Backend>(f: &mut Frame<B>, app: &App) {
    let data_slice = &app.vec_highest_transactions[..];

    let chunks_2 = Layout::default()
        .direction(Direction::Vertical)
        .margin(0)
        .constraints(
            [
                Constraint::Percentage(15),
                Constraint::Ratio(5, 5),
                Constraint::Max(5),
                Constraint::Min(0),
            ]
            .as_ref(),
        )
        .split(f.size());

    let data_sets = vec![Dataset::default()
        .name("valeurs")
        .marker(symbols::Marker::Dot)
        .graph_type(GraphType::Scatter)
        .style(Style::default().fg(Color::Cyan))
        .data(data_slice)];

    let half = (app.vec_highest_transactions.len() / 2).to_string();
    let end = app.vec_highest_transactions.len().to_string();

    let quarter_y = (app.highest_value_file / 4.0).trunc().to_string();
    let end_y = app.highest_value_file.round().to_string();
    let three_quarter = (3.0 * app.highest_value_file / 4.0)
        .trunc()
        .to_string();
    let half_y = (app.highest_value_file / 2.0).trunc().to_string();

    let chart = Chart::new(data_sets)
        .block(
            Block::default()
                .borders(Borders::ALL)
                .title("Plus grosse valeur par bloc"),
        )
        .x_axis(
            Axis::default()
                .title(Span::styled("Blocs", Style::default().fg(Color::Red)))
                .style(Style::default().fg(Color::Black))
                .bounds([0.0, app.vec_highest_transactions.len() as f64])
                .labels(
                    ["0", half.as_str(), end.as_str()]
                        .iter()
                        .cloned()
                        .map(Span::from)
                        .collect(),
                ),
        )
        .y_axis(
            Axis::default()
                .title(Span::styled(
                    "Valeurs en Bitcoin",
                    Style::default().fg(Color::Red),
                ))
                .style(Style::default().fg(Color::Black))
                .bounds([0.0, app.highest_value_file])
                .labels(
                    [
                        "0",
                        quarter_y.as_str(),
                        half_y.as_str(),
                        three_quarter.as_str(),
                        end_y.as_str(),
                    ]
                    .iter()
                    .cloned()
                    .map(Span::from)
                    .collect(),
                ),
        );

    let rect_1 = Rect::new(
        chunks_2[1].x,
        chunks_2[1].y,
        chunks_2[1].width,
        chunks_2[1].height,
    );
    f.render_widget(chart, rect_1);
}

fn help_tabs_4<B: Backend>(f: &mut Frame<B>, chunks: &Vec<Rect>) {
    let info1: &str = "- Pour naviguer entre les onglets : fleche gauche ou droite";
    let info2: &str = "- Pour défiler les listes (status/verification MR) : fleche bas ou haut";
    let info3: &str = "- Pour sortir : q/Q ";
    let info4: &str = "- Pour passer au fichier suivant : s/S";
    let info5: &str = "- Pour passer au fichier précédent : p";
    let info6: &str = "- Pour passer au fichier précédent et écrire : P";
    let info7: &str = "- Pour afficher l'aide : h/H";
    let info8: &str = "- Pour changer de bloc selectionné : +/-";
    let info9: &str = "- Pour exporter le fichier et passer au fichier suivant : e ";
    let info10: &str = "- Pour exporter le fichier et sortir de l'application : E ";

    let text = vec![
        Spans::from(info1),
        Spans::from(info2),
        Spans::from(info3),
        Spans::from(info4),
        Spans::from(info5),
        Spans::from(info6),
        Spans::from(info7),
        Spans::from(info8),
        Spans::from(info9),
        Spans::from(info10),
    ];
    let paragraph = Paragraph::new(text)
        .block(Block::default().borders(Borders::ALL).title("Aide"))
        .alignment(Alignment::Left)
        .wrap(Wrap { trim: true });

    let rect_2 = Rect::new(chunks[1].x, chunks[1].y, chunks[1].width, chunks[1].height);
    f.render_widget(paragraph, rect_2);
}

pub fn write_waiting<B: Backend>(f: &mut Frame<B>, val_loop: &mut u16, name_file: String) {
    let chunks_2 = Layout::default()
        .direction(Direction::Vertical)
        .vertical_margin(0)
        .constraints(
            [
                Constraint::Percentage(45),
                Constraint::Ratio(1, 6),
                Constraint::Max(5),
                Constraint::Min(0),
            ]
            .as_ref(),
        )
        .split(f.size());

    let mut title = String::from("Ecriture ");
    title.push_str(&name_file.as_str());

    let gauge = Gauge::default()
        .block(Block::default().title(title).borders(Borders::ALL))
        .gauge_style(match val_loop {
            0..=25 => Style::default().fg(Color::Red),
            26..=50 => Style::default().fg(Color::Rgb(215, 110, 22)),
            51..=60 => Style::default().fg(Color::Rgb(211, 194, 8)),
            61..=75 => Style::default().fg(Color::Rgb(150, 197, 16)),
            76..=100 => Style::default().fg(Color::Green),
            _ => Style::default(),
        })
        .percent(*val_loop);

    let rect_1 = Rect::new(
        chunks_2[1].x,
        chunks_2[1].y,
        chunks_2[1].width,
        chunks_2[1].height,
    );
    f.render_widget(gauge, rect_1);
}

pub fn parser_waiting<B: Backend>(f: &mut Frame<B>, val_loop: &mut u16, name_file: String) {
    let chunks_2 = Layout::default()
        .direction(Direction::Vertical)
        .vertical_margin(0)
        .constraints(
            [
                Constraint::Percentage(45),
                Constraint::Ratio(1, 6),
                Constraint::Max(5),
                Constraint::Min(0),
            ]
            .as_ref(),
        )
        .split(f.size());

    let mut title = String::from("Lecture fichier ");
    title.push_str(&name_file.as_str());

    let gauge = Gauge::default()
        .block(Block::default().title(title).borders(Borders::ALL))
        .gauge_style(match val_loop {
            0..=25 => Style::default().fg(Color::Red),
            26..=50 => Style::default().fg(Color::Rgb(215, 110, 22)),
            51..=60 => Style::default().fg(Color::Rgb(211, 194, 8)),
            61..=75 => Style::default().fg(Color::Rgb(150, 197, 16)),
            76..=100 => Style::default().fg(Color::Green),
            _ => Style::default(),
        })
        .percent(*val_loop);

    let rect_1 = Rect::new(
        chunks_2[1].x,
        chunks_2[1].y,
        chunks_2[1].width,
        chunks_2[1].height,
    );
    f.render_widget(gauge, rect_1);
}

pub fn calcul_merkle_root_attente<B: Backend>(
    f: &mut Frame<B>,
    val_loop: &mut u16,
    name_file: String,
) {
    let chunks_2 = Layout::default()
        .direction(Direction::Vertical)
        .vertical_margin(0)
        .constraints(
            [
                Constraint::Percentage(45),
                Constraint::Ratio(1, 6),
                Constraint::Max(5),
                Constraint::Min(0),
            ]
            .as_ref(),
        )
        .split(f.size());

    let mut title = String::from("Mise en place des données ");
    title.push_str(&name_file.as_str());

    let gauge = Gauge::default()
        .block(Block::default().title(title).borders(Borders::ALL))
        .gauge_style(match val_loop {
            0..=25 => Style::default().fg(Color::Red),
            26..=50 => Style::default().fg(Color::Rgb(215, 110, 22)),
            51..=60 => Style::default().fg(Color::Rgb(211, 194, 8)),
            61..=75 => Style::default().fg(Color::Rgb(150, 197, 16)),
            76..=100 => Style::default().fg(Color::Green),
            _ => Style::default(),
        })
        .percent(*val_loop);

    let rect_1 = Rect::new(
        chunks_2[1].x,
        chunks_2[1].y,
        chunks_2[1].width,
        chunks_2[1].height,
    );
    f.render_widget(gauge, rect_1);
}
